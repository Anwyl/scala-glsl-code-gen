# Usage
Code listed here assumes the following unusual imports:
```
import com.jwinn.glsl.codegen.GLSLImplicits._
import com.jwinn.glsl.codegen.datastructures.VarType._
```

To generate a GLSL string, use the ShaderContent object. It must at least have a main method set using ShaderContent.setMain

Example:
```
val sc = new ShaderContent
sc.setMain { implicit ctx ⇒
  val localVar1 = INT("localVar1")
  val localVar2 = INT("localVar2")
  (localVar1 := localVar1 + localVar2).`;`
}
sc.definition
```

sc.definition in this case would generate a shader similar to this:
```
#version 430
void main(void) { 
  int localVar1;
  int localVar2;
  localVar1 = (localVar1 + localVar2);
}
```

Local variables are defined using VarType.GLSL_TYPE_NAME.

Example:
```
val sc = new ShaderContent
sc.setMain { implicit ctx ⇒
  val localVar1 = UINT("localVar1")
  val localVar2 = INT("localVar2", 2)
  (localVar1 := localVar1 + localVar2).`;`
}
sc.definition
```

sc.definition in this case would generate a shader similar to this:
```
#version 430
void main(void) { 
  uint localVar1;
  int localVar2 = 2;
  localVar1 = (localVar1 + localVar2);
}
```

All code is ordinary scala, so normal loops are unrolled.

Example:
```
val sc = new ShaderContent
sc.setMain { implicit ctx ⇒
  val localVar1 = INT("localVar1")
  val localVar2 = INT("localVar2")
  for (_ <- 0 until 2) {
    (localVar1 := localVar1 + localVar2).`;`
  }
}
sc.definition
```

generates

```
#version 430
void main(void) { 
  int localVar1;
  int localVar2;
  localVar1 = (localVar1 + localVar2);
  localVar1 = (localVar1 + localVar2);
}
```

In order to generate GLSL loops, use GLFor and GLWhile.

Example:
```
val sc = new ShaderContent
sc.setMain { implicit ctx ⇒
  val localVar1 = INT("localVar1")
  val localVar2 = INT("localVar2")
  GLFor ("i", 0, 2) { (context: FunctionContent[_], i) =>
    implicit val ctx: FunctionContent[_] = context
    (localVar1 := localVar1 + localVar2).`;`
  }
}
sc.definition
```

generates

```
#version 430
void main(void) { 
  int localVar1;
  int localVar2;
  for (int i = 0; i < 2; i++) {
    localVar1 = (localVar1 + localVar2);
  }
}
```

Globals can be defined using VarDef


Example:
```
val sc = new ShaderContent
val globalVar1 = VarDef(AccessType.UNIFORM, MemberDef("globalVar1", INT))
val globalVar2 = VarDef(AccessType.IN, MemberDef("globalVar2", INT))
val globalVar3 = VarDef(AccessType.OUT, MemberDef("globalVar3", INT))
sc.setMain { implicit ctx ⇒
  val g1 = globalVar1.inContext
  val g2 = globalVar2.inContext
  val g3 = globalVar3.inContext
  (g3 := g1 + g2).`;`
}
sc.definition
```

generates

```
#version 430

uniform int globalVar1;
in int globalVar2;
out int globalVar3;

void main(void){
  globalVar3 = globalVar1 + globalVar2;
}
```
