package com.jwinn.glsl.codegen

import com.jwinn.glsl.codegen.datastructures.HasValue
import com.jwinn.glsl.codegen.types.{GLSLBool, GLSLDMat2, GLSLDMat3, GLSLDMat4, GLSLDVec2, GLSLDVec3, GLSLDVec4, GLSLDouble, GLSLFloat, GLSLIVec2, GLSLIVec3, GLSLIVec4, GLSLInt, GLSLMat2, GLSLMat3, GLSLMat4, GLSLUInt, GLSLUVec2, GLSLUVec3, GLSLUVec4, GLSLValue, GLSLVec2, GLSLVec3, GLSLVec4}

object GLSLImplicits {
  implicit val uintToGLSL  : Int     ⇒ GLSLUInt   = new GLSLUInt  (_)
  implicit val intToGLSL   : Int     ⇒ GLSLInt    = new GLSLInt   (_)
  implicit val floatToGLSL : Float   ⇒ GLSLFloat  = new GLSLFloat (_)
  implicit val doubleToGLSL: Double  ⇒ GLSLDouble = new GLSLDouble(_)
  implicit val boolToGlsl  : Boolean ⇒ GLSLBool   = new GLSLBool  (_)

  implicit val intToUint    : GLSLInt   ⇒ GLSLUInt   = in ⇒ new GLSLUInt   (in.glsl)

  implicit val uintToFloat  : GLSLUInt  ⇒ GLSLFloat  = in ⇒ new GLSLFloat  (in.glsl)
  implicit val intToFloat   : GLSLInt   ⇒ GLSLFloat  = in ⇒ new GLSLFloat  (in.glsl)

  implicit val uintToDouble : GLSLUInt  ⇒ GLSLDouble = in ⇒ new GLSLDouble(in.glsl)
  implicit val intToDouble  : GLSLInt   ⇒ GLSLDouble = in ⇒ new GLSLDouble(in.glsl)
  implicit val floatToDouble: GLSLFloat ⇒ GLSLDouble = in ⇒ new GLSLDouble(in.glsl)

  implicit val iVec2ToUVec2 : GLSLIVec2 ⇒ GLSLUVec2  = in ⇒ new GLSLUVec2 (in.glsl)
  implicit val iVec3ToUVec3 : GLSLIVec3 ⇒ GLSLUVec3  = in ⇒ new GLSLUVec3 (in.glsl)
  implicit val iVec4ToUVec4 : GLSLIVec4 ⇒ GLSLUVec4  = in ⇒ new GLSLUVec4 (in.glsl)

  implicit val ivec2ToVec2  : GLSLIVec2 ⇒ GLSLVec2   = in ⇒ new GLSLVec2  (in.glsl)
  implicit val ivec3ToVec3  : GLSLIVec3 ⇒ GLSLVec3   = in ⇒ new GLSLVec3  (in.glsl)
  implicit val ivec4ToVec4  : GLSLIVec4 ⇒ GLSLVec4   = in ⇒ new GLSLVec4  (in.glsl)
  implicit val uvec2ToVec2  : GLSLUVec2 ⇒ GLSLVec2   = in ⇒ new GLSLVec2  (in.glsl)
  implicit val uvec3ToVec3  : GLSLUVec3 ⇒ GLSLVec3   = in ⇒ new GLSLVec3  (in.glsl)
  implicit val uvec4ToVec4  : GLSLUVec4 ⇒ GLSLVec4   = in ⇒ new GLSLVec4  (in.glsl)

  implicit val ivec2ToDVec2 : GLSLIVec2 ⇒ GLSLDVec2  = in ⇒ new GLSLDVec2 (in.glsl)
  implicit val ivec3ToDVec3 : GLSLIVec3 ⇒ GLSLDVec3  = in ⇒ new GLSLDVec3 (in.glsl)
  implicit val ivec4ToDVec4 : GLSLIVec4 ⇒ GLSLDVec4  = in ⇒ new GLSLDVec4 (in.glsl)
  implicit val uvec2ToDVec2 : GLSLUVec2 ⇒ GLSLDVec2  = in ⇒ new GLSLDVec2 (in.glsl)
  implicit val uvec3ToDVec3 : GLSLUVec3 ⇒ GLSLDVec3  = in ⇒ new GLSLDVec3 (in.glsl)
  implicit val uvec4ToDVec4 : GLSLUVec4 ⇒ GLSLDVec4  = in ⇒ new GLSLDVec4 (in.glsl)
  implicit val vec2ToDVec2  : GLSLVec2  ⇒ GLSLDVec2  = in ⇒ new GLSLDVec2 (in.glsl)
  implicit val vec3ToDVec3  : GLSLVec3  ⇒ GLSLDVec3  = in ⇒ new GLSLDVec3 (in.glsl)
  implicit val vec4ToDVec4  : GLSLVec4  ⇒ GLSLDVec4  = in ⇒ new GLSLDVec4 (in.glsl)

  implicit val mat2ToDMat2  : GLSLMat2  ⇒ GLSLDMat2  = in ⇒ new GLSLDMat2 (in.glsl)
  implicit val mat3ToDMat3  : GLSLMat3  ⇒ GLSLDMat3  = in ⇒ new GLSLDMat3 (in.glsl)
  implicit val mat4ToDMat4  : GLSLMat4  ⇒ GLSLDMat4  = in ⇒ new GLSLDMat4 (in.glsl)

  implicit def convert[T <: GLSLValue[T]](h: HasValue[T])(implicit ctx: FunctionContent[_]): T = h.value
}
