package com.jwinn.glsl.codegen

import com.jwinn.glsl.codegen.structures.MainFunction

/**
 * Content of a shader.
 * Includes any global variables, helper functions, and a single main function.
 *
 * Example of usage:
 * {{{
 * val input  : MemberDef[GLSLArray[GLSLDVec4]] = new ARRAY[GLSLDVec4](DVEC4)       ("input")
 * val outBuf : MemberDef[GLSLArray[GLSLDVec4]] = new ARRAY[GLSLDVec4](DVEC4)       ("outBuf")
 *
 * shaderContent.addBufferBlock(BufferBlock(AccessType.BUFFER, "Input",  Array(input),  None, Seq(READ_ONLY)))
 * shaderContent.addBufferBlock(BufferBlock(AccessType.BUFFER, "Output", Array(outBuf), None, Seq(WRITE_ONLY)))
 *
 * shaderContent.setMain(context ⇒ {
 *   import scala.language.postfixOps
 *   implicit val ctx: FunctionContent[Nothing] = context
 *   implicit val intToGLSL: Int ⇒ GLSLUInt = new GLSLUInt(_)
 *   implicit val doubleToGLSL: Double ⇒ GLSLDouble = new GLSLDouble(_)
 *   val base = UINT("base", gl_WorkGroupID.x * (gl_NumWorkGroups.x - 1)).`;`
 *   val accumulator = DVEC4("accumulator", dvec4(0d)).`;`
 *   GLFor[Nothing]("i" 0, gl_NumWorkGroups.x - 1)((context, i) ⇒ {
 *     implicit val ctx: FunctionContent[Nothing] = context
 *     (accumulator += input.value(base + i)).`;`
 *   }).`;`
 *   (outBuf.value(gl_WorkGroupID.x) := accumulator).`;`
 * })
 * shaderContent.definition
 * }}}
 * This will produce GLSL equivalent to:
 * {{{
 * #version 430
 * layout(std430) buffer;
 * layout(local_size_x = 1) in;
 *
 * readonly buffer Input {
 *   dvec4[] input;
 * };
 * writeonly buffer Output {
 *   dvec4[] outBuf;
 * };
 *
 * void main(void){
 *   uint base = gl_WorkGroupID.x * (gl_NumWorkGroups.x - 1);
 *   dvec4 accumulator = dvec4(0.0);
 *   for (uint i = 0; i < gl_NumWorkGroups.x - 1; i++) {
 *     accumulator += input[base + i];
 *   }
 *   outBuf[gl_WorkGroupID.x] = accumulator;
 * }
 * }}}
 * @param version GLSL version string. Defaults to version 4.3
 * @param layout GLSL default layout and precision strings. Defaults to std430 layout with highp floats
 */
class ShaderContent(private val version: String = "#version 430", private val layout: String = "layout(std430) buffer;precision highp float") {

  val dependencies: GlobalDependencies = new GlobalDependencies

  private var main: Option[MainFunction] = None

  /**
   * Sets the main function for this shader.
   * Should only be called once per instance.
   * @param main The main function.
   */
  def setMain(main: MainFunction): Unit = {
    dependencies.addAll(main.context.dependencies)
    this.main = Some(main)
  }

  /**
   * Convenience method to set the main function to a block of code rather than an explicit function
   * @param body Lines which constitute the body of the main function.
   */
  def setMain(body: FunctionContent[Nothing] ⇒ Unit): Unit = setMain(MainFunction(body))

  /**
   * @return The GLSL code for this shader.
   */
  def definition: String = {
    val preamble = version + "\n" + layout + ";"
    val globalPart = dependencies.definition
    val mainPart   = main.map(_.definition + GLWhitespace.NEWLINE) .mkString
    Seq(preamble, globalPart, mainPart).mkString(GLWhitespace.NEWLINE)
  }
}
