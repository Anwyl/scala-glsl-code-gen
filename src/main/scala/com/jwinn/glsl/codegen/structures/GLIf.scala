package com.jwinn.glsl.codegen.structures

import com.jwinn.glsl.codegen.types.{GLSLBool, GLSLValue}
import com.jwinn.glsl.codegen.{FunctionContent, GLWhitespace}

/**
 * GLSL "if" statement
 * @param test The conditional part of the statement. For instance "i < 4"
 * @param lines The code to execute if the condition is true
 */
case class GLIf(test: GLSLBool)(lines: FunctionContent[Nothing] ⇒ Unit)(implicit parent: FunctionContent[_]) extends GLSLBlock {

  private val content: FunctionContent[Nothing] = new FunctionContent
  lines(content)
  parent.addBlock(this)
  def context: Seq[FunctionContent[_]] = Seq(content)
  def definition: String = "if(" + test.glsl + "){" + GLWhitespace.NEWLINE + content.definition + GLWhitespace.TAB + "}" + GLWhitespace.NEWLINE
}

/**
 * GLSL "if" statement which ends with a return statement. This allows more scala-like returns within an if block.
 * @param test The conditional part of the statement. For instance "i < 4"
 * @param lines The code to execute if the condition is true
 * @tparam T The return type of the function
 */
case class GLIfR[T <: GLSLValue[T]](test: GLSLBool)(lines: FunctionContent[T] ⇒ T)(implicit parent: FunctionContent[T]) extends GLSLBlock {

  private val content: FunctionContent[T] = new FunctionContent
  content.returnVal(lines(content))
  parent.addBlock(this)
  def context: Seq[FunctionContent[_]] = Seq(content)
  def definition: String = "if(" + test.glsl + "){" + GLWhitespace.NEWLINE + content.definition + GLWhitespace.TAB + "}" + GLWhitespace.NEWLINE
}

/**
 * GLSL "if/else" statement
 * @param test The conditional part of the statement. For instance "i < 4"
 * @param lines The code to execute if the condition is true
 * @param elseLines The code to execute if the condition is false
 */
case class GLIfE(test: GLSLBool)(lines: FunctionContent[Nothing] ⇒ Unit)(elseLines: FunctionContent[Nothing] ⇒ Unit)(implicit parent: FunctionContent[_]) extends GLSLBlock {

  private val content: FunctionContent[Nothing] = new FunctionContent
  lines(content)
  private val elseContent: FunctionContent[Nothing] = new FunctionContent
  elseLines(elseContent)
  parent.addBlock(this)
  def context: Seq[FunctionContent[_]] = Seq(content, elseContent)
  def definition: String = "if(" + test.glsl + "){" + GLWhitespace.NEWLINE + content.definition + GLWhitespace.TAB + "}else{" + elseContent.definition + GLWhitespace.TAB + "}" + GLWhitespace.NEWLINE
}