package com.jwinn.glsl.codegen.structures

import com.jwinn.glsl.codegen.datastructures.VarType.UINT
import com.jwinn.glsl.codegen.datastructures.{MemberDef, VarType}
import com.jwinn.glsl.codegen.{FunctionContent, GLWhitespace}
import com.jwinn.glsl.codegen.types.{GLSLBool, GLSLUInt, GLSLValue}

object GLFor {
  def apply[ReturnType <: GLSLValue[ReturnType]](varName: String, initialValue: Int, maxValue: Int)(lines: (FunctionContent[ReturnType], GLSLUInt) ⇒ Unit)(implicit parent: FunctionContent[ReturnType]): Unit = {
    import com.jwinn.glsl.codegen.GLSLImplicits._
    import scala.language.postfixOps
    if (initialValue + 1 < maxValue) {
      GLFor[GLSLUInt, ReturnType](UINT, varName, initialValue, _ < maxValue, _ ++)(lines)
    } else if (initialValue < maxValue) {
      lines(parent, initialValue)
    }
  }
}

/**
 * GLSL "for" statement which creates a single variable within the for statement.
 * @param varType The type of the loop variable
 * @param name The name of the loop variable
 * @param test The test part of the for statement. Typically something like "i < length"
 * @param thirdPart The iterated part of the for statement. Typically something like "i++"
 * @param lines The content of the for block
 * @tparam T The type of the variable created in the first part of the for statement
 */
case class GLFor[T <: GLSLValue[T], ReturnType <: GLSLValue[ReturnType]](varType: VarType[T], name: String, initialValue: T, test: T ⇒ GLSLBool, thirdPart: T ⇒ GLSLValue[_])(lines: (FunctionContent[ReturnType], T) ⇒ Unit)(implicit parent: FunctionContent[ReturnType]) extends GLSLBlock {
  private val content: FunctionContent[ReturnType] = new FunctionContent[ReturnType]
  private val memberDef = MemberDef(name, varType, Some(initialValue))
  private val inValue = memberDef.value
  lines(content, inValue)
  parent.addBlock(this)
  def context: Seq[FunctionContent[_]] = Seq(content)
  def definition: String = "for(" + memberDef.definition + ";" + test(inValue).glsl + ";" + thirdPart(inValue).glsl + ") {" + GLWhitespace.NEWLINE + content.definition + GLWhitespace.TAB + "}" + GLWhitespace.NEWLINE
}
