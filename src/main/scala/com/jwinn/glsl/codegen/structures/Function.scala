package com.jwinn.glsl.codegen.structures

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.{FunctionContent, GLWhitespace}
import com.jwinn.glsl.codegen.types.GLSLValue

/**
 * Parameter of a function
 * @param name Name of the parameter
 * @param paramType Type of the parameter
 * @tparam Type VarType of the parameter
 */
case class Param[Type <: GLSLValue[Type]](name: String, paramType: VarType[Type], in: Boolean = true, out: Boolean = false) {
  val definition: String = (if (in) "in" else "") + (if (out) "out" else "") + " " + paramType.typeName + " " + name
}

/**
 * A GLSL function
 * @param name Name of the function
 * @param returnType Return type of the function
 * @param params List of parameters of the function
 * @param body Body of the function
 * @tparam ReturnType Return type of the function
 */
abstract class Function[ReturnType <: GLSLValue[ReturnType]](name: String, returnType: VarType[ReturnType], params: Seq[Param[_]])(body: FunctionContent[ReturnType] ⇒ ReturnType) {
  val context = new FunctionContent[ReturnType]
  context.returnVal(body(context))
  val definition: String = returnType.typeName + " " + name + "(" + params.map(_.definition).mkString(",") + "){" + GLWhitespace.NEWLINE + context.definition + "}"
  protected def call(context: Option[FunctionContent[_]], params: Seq[GLSLValue[_]]): ReturnType = {
    context.foreach(_.dependencies.addFunction(this))
    returnType.factory(name + "(" + params.map(_.glsl).mkString(",") + ")")
  }
}

/**
 * Main function of a shader.
 * Always has 0 arguments, and no return type.
 * @param body Body of the function
 */
case class MainFunction(body: FunctionContent[Nothing] ⇒ Unit) {
  val context = new FunctionContent[Nothing]
  body(context)
  val definition: String =  "void main(void){" + GLWhitespace.NEWLINE + context.definition + "}"
}

/**
 * Function which has 0 parameters
 * @param name Name of the function
 * @param returnType Return type of the function
 * @param body Body of the function
 * @tparam ReturnType Return type of the function
 */
case class Function0[ReturnType <: GLSLValue[ReturnType]](name: String, returnType: VarType[ReturnType])(body: FunctionContent[ReturnType] ⇒ ReturnType = (_: FunctionContent[ReturnType]) ⇒ returnType.factory("")) extends Function(name, returnType, Seq())(body) {
  def apply[T <: GLSLValue[T]]()(implicit context: FunctionContent[T]): ReturnType = call(Some(context), Seq())
  def applyBuiltin(): ReturnType = call(None, Seq())
}

/**
 * Function which has 1 parameter
 * @param name Name of the function
 * @param returnType Return type of the function
 * @param param1 First parameter
 * @param body Body of the function
 * @tparam ReturnType Return type of the function
 * @tparam Param1Type Type of the parameter
 */
case class Function1[ReturnType <: GLSLValue[ReturnType], Param1Type <: GLSLValue[Param1Type]](name: String, returnType: VarType[ReturnType], param1: Param[Param1Type])(body: (FunctionContent[ReturnType], Param1Type) ⇒ ReturnType = (_: FunctionContent[ReturnType], _: Param1Type) ⇒ returnType.factory("")) extends Function(name, returnType, Seq(param1))(body(_, param1.paramType.factory(param1.name))) {
  def apply[T <: GLSLValue[T]](param1: Param1Type)(implicit context: FunctionContent[T]): ReturnType = call(Some(context), Seq(param1))
  def applyBuiltin(param1: Param1Type): ReturnType = call(None, Seq(param1))
}

/**
 * Function with 2 parameters
 * @param name Name of the function
 * @param returnType Return type of the function
 * @param param1 First parameter
 * @param param2 Second Parameter
 * @param body Body of the function
 * @tparam ReturnType Return type of the function
 * @tparam Param1Type Type of the first parameter
 * @tparam Param2Type Type of the second parameter
 */
case class Function2[ReturnType <: GLSLValue[ReturnType], Param1Type <: GLSLValue[Param1Type], Param2Type <: GLSLValue[Param2Type]](name: String, returnType: VarType[ReturnType], param1: Param[Param1Type], param2: Param[Param2Type])(body: (FunctionContent[ReturnType], Param1Type, Param2Type) ⇒ ReturnType = (_: FunctionContent[ReturnType], _: Param1Type, _: Param2Type) ⇒ returnType.factory("")) extends Function(name, returnType, Seq(param1, param2))(body(_, param1.paramType.factory(param1.name), param2.paramType.factory(param2.name))) {
  def apply[T <: GLSLValue[T]](param1: Param1Type, param2: Param2Type)(implicit context: FunctionContent[T]): ReturnType = call(Some(context), Seq(param1, param2))
  def applyBuiltin(param1: Param1Type, param2: Param2Type): ReturnType = call(None, Seq(param1, param2))
}

/**
 * Function with 3 parameters
 * @param name Name of the function
 * @param returnType Return type of the function
 * @param param1 First parameter
 * @param param2 Second parameter
 * @param param3 Third parameter
 * @param body Body of the function
 * @tparam ReturnType Return type of the function
 * @tparam Param1Type Type of the first parameter
 * @tparam Param2Type Type of the second parameter
 * @tparam Param3Type Type of the third parameter
 */
case class Function3[ReturnType <: GLSLValue[ReturnType], Param1Type <: GLSLValue[Param1Type], Param2Type <: GLSLValue[Param2Type], Param3Type <: GLSLValue[Param3Type]](name: String, returnType: VarType[ReturnType], param1: Param[Param1Type], param2: Param[Param2Type], param3: Param[Param3Type])(body: (FunctionContent[ReturnType], Param1Type, Param2Type, Param3Type) ⇒ ReturnType = (_: FunctionContent[ReturnType], _: Param1Type, _: Param2Type, _: Param3Type) ⇒ returnType.factory("")) extends Function(name, returnType, Seq(param1, param2, param3))(body(_, param1.paramType.factory(param1.name), param2.paramType.factory(param2.name), param3.paramType.factory(param3.name))) {
  def apply[T <: GLSLValue[T]](param1: Param1Type, param2: Param2Type, param3: Param3Type)(implicit context: FunctionContent[T]): ReturnType = call(Some(context), Seq(param1, param2, param3))
  def applyBuiltin(param1: Param1Type, param2: Param2Type, param3: Param3Type): ReturnType = call(None, Seq(param1, param2, param3))
}

/**
 * Function with 4 parameters
 * @param name Name of the function
 * @param returnType Return type of the function
 * @param param1 First parameter
 * @param param2 Second parameter
 * @param param3 Third parameter
 * @param param4 Fourth parameter
 * @param body Body of the function
 * @tparam ReturnType Return type of the function
 * @tparam Param1Type Type of the first parameter
 * @tparam Param2Type Type of the second parameter
 * @tparam Param3Type Type of the third parameter
 * @tparam Param4Type Type of the fourth parameter
 */
case class Function4[ReturnType <: GLSLValue[ReturnType], Param1Type <: GLSLValue[Param1Type], Param2Type <: GLSLValue[Param2Type], Param3Type <: GLSLValue[Param3Type], Param4Type <: GLSLValue[Param4Type]](name: String, returnType: VarType[ReturnType], param1: Param[Param1Type], param2: Param[Param2Type], param3: Param[Param3Type], param4: Param[Param4Type])(body: (FunctionContent[ReturnType], Param1Type, Param2Type, Param3Type, Param4Type) ⇒ ReturnType = (_: FunctionContent[ReturnType], _: Param1Type, _: Param2Type, _: Param3Type, _: Param4Type) ⇒ returnType.factory("")) extends Function(name, returnType, Seq(param1, param2, param3, param4))(body(_, param1.paramType.factory(param1.name), param2.paramType.factory(param2.name), param3.paramType.factory(param3.name), param4.paramType.factory(param4.name))) {
  def apply[T <: GLSLValue[T]](param1: Param1Type, param2: Param2Type, param3: Param3Type, param4: Param4Type)(implicit context: FunctionContent[T]): ReturnType = call(Some(context), Seq(param1, param2, param3, param4))
  def applyBuiltin(param1: Param1Type, param2: Param2Type, param3: Param3Type, param4: Param4Type): ReturnType = call(None, Seq(param1, param2, param3, param4))
}

/**
 *
 * @param name Name of the function
 * @param returnType Return type of the function
 * @param param1 First parameter
 * @param param2 Second parameter
 * @param param3 Third parameter
 * @param param4 Fourth parameter
 * @param param5 Fifth parameter
 * @param body Body of the function
 * @tparam ReturnType Return type of the function
 * @tparam Param1Type Type of the first parameter
 * @tparam Param2Type Type of the second parameter
 * @tparam Param3Type Type of the third parameter
 * @tparam Param4Type Type of the fourth parameter
 * @tparam Param5Type Type of the fifth parameter
 */
case class Function5[ReturnType <: GLSLValue[ReturnType], Param1Type <: GLSLValue[Param1Type], Param2Type <: GLSLValue[Param2Type], Param3Type <: GLSLValue[Param3Type], Param4Type <: GLSLValue[Param4Type], Param5Type <: GLSLValue[Param5Type]](name: String, returnType: VarType[ReturnType], param1: Param[Param1Type], param2: Param[Param2Type], param3: Param[Param3Type], param4: Param[Param4Type], param5: Param[Param5Type])(body: (FunctionContent[ReturnType], Param1Type, Param2Type, Param3Type, Param4Type, Param5Type) ⇒ ReturnType = (_: FunctionContent[ReturnType], _: Param1Type, _: Param2Type, _: Param3Type, _: Param4Type, _: Param5Type) ⇒ returnType.factory("")) extends Function(name, returnType, Seq(param1, param2, param3, param4, param5))(body(_, param1.paramType.factory(param1.name), param2.paramType.factory(param2.name), param3.paramType.factory(param3.name), param4.paramType.factory(param4.name), param5.paramType.factory(param5.name))) {
  def apply[T <: GLSLValue[T]](param1: Param1Type, param2: Param2Type, param3: Param3Type, param4: Param4Type, param5: Param5Type)(implicit context: FunctionContent[T]): ReturnType = call(Some(context), Seq(param1, param2, param3, param4, param5))
  def applyBuiltin(param1: Param1Type, param2: Param2Type, param3: Param3Type, param4: Param4Type, param5: Param5Type): ReturnType = call(None, Seq(param1, param2, param3, param4, param5))
}
