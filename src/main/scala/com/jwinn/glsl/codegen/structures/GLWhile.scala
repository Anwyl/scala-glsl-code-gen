package com.jwinn.glsl.codegen.structures

import com.jwinn.glsl.codegen.types.GLSLBool
import com.jwinn.glsl.codegen.{FunctionContent, GLWhitespace}

/**
 * GLSL while loop
 * @param test The test part of the while loop definition. E.G. "x < 5"
 * @param lines The content of the while loop
 * @tparam T the return type of the containing function
 */
case class GLWhile(test: GLSLBool)(lines: FunctionContent[Nothing] ⇒ Unit)(implicit parent: FunctionContent[_]) extends GLSLBlock {
  private val content: FunctionContent[Nothing] = new FunctionContent[Nothing]
  lines(content)
  parent.addBlock(this)
  def context: Seq[FunctionContent[_]] = Seq(content)
  def definition: String = "while(" + test.glsl + ") {" + GLWhitespace.NEWLINE + content.definition + GLWhitespace.TAB + "}" + GLWhitespace.NEWLINE
}
