package com.jwinn.glsl.codegen.structures

import com.jwinn.glsl.codegen.FunctionContent

/**
 * A block of GLSL code
 */
trait GLSLBlock {
  def definition: String
  def context: Seq[FunctionContent[_]]
}
