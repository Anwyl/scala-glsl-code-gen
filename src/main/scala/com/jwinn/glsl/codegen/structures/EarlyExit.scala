package com.jwinn.glsl.codegen.structures

import com.jwinn.glsl.codegen.{FunctionContent, GLWhitespace}
import com.jwinn.glsl.codegen.types.GLSLValue

/**
 * Statements which cause an early exit from a function. Includes return statements and "discard" in fragment shaders.
 */
object EarlyExit {
  /**
   * Discards a fragment without writing to any buffers
   * @param context context in which this statement appears
   */
  def GLDiscard(implicit context: FunctionContent[_]): Unit = context.addBlock(new GLSLBlock {
    override def definition: String = "discard;" + GLWhitespace.NEWLINE
    override def context: Seq[FunctionContent[_]] = Seq()
  })

  /**
   * Returns a value
   * @param value Value returned
   * @param context Context in which this statement appears
   * @tparam T Return value's type
   */
  def GLReturn[T <: GLSLValue[T]](value: T)(implicit context: FunctionContent[T]): Unit = context.addBlock(new GLSLBlock {
    override def definition: String = "return " + value.glsl + ";" + GLWhitespace.NEWLINE
    override def context: Seq[FunctionContent[_]] = Seq()
  })

  /**
   * Return without a value
   * @param context Context in which this statement appears
   */
  def GLReturn(implicit context: FunctionContent[Nothing]): Unit = context.addBlock(new GLSLBlock {
    override def definition: String = "return;" + GLWhitespace.NEWLINE
    override def context: Seq[FunctionContent[_]] = Seq()
  })
}
