package com.jwinn.glsl.codegen.structures

import com.jwinn.glsl.codegen.types.GLSLValue
import com.jwinn.glsl.codegen.{FunctionContent, GLWhitespace}

/**
 * GLSL scoping block.
 * @param lines The content of the for block
 */
case class CodeBlock[T <: GLSLValue[T]](lines: FunctionContent[T] ⇒ Unit)(implicit parent: FunctionContent[T]) extends GLSLBlock {
  private val content: FunctionContent[T] = new FunctionContent[T]
  lines(content)
  parent.addBlock(this)
  def context: Seq[FunctionContent[T]] = Seq(content)
  def definition: String = "{" + GLWhitespace.NEWLINE + content.definition + GLWhitespace.TAB + "}" + GLWhitespace.NEWLINE
}
