package com.jwinn.glsl.codegen

import com.jwinn.glsl.codegen.datastructures.VarType.FLOAT
import com.jwinn.glsl.codegen.types._

/**
 * Built-in variables available within vertex shaders
 */
object VertShaderBuiltins {
  //in
  val gl_VertexID = new GLSLInt("gl_VertexID")
  val gl_InstanceID = new GLSLInt("gl_InstanceID")
  val gl_DrawID = new GLSLInt("gl_DrawID")
  val gl_BaseVertex = new GLSLInt("gl_BaseVertex")
  val gl_BaseInstance = new GLSLInt("gl_BaseInstance")

  //out
  val gl_PointSize = new GLSLFloat("gl_PointSize")
  val gl_Position = new GLSLVec4("gl_Position")
  val gl_ClipDistance = new GLSLArray("gl_ClipDistance", FLOAT)
}
