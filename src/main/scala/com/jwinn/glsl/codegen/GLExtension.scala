package com.jwinn.glsl.codegen

object GLExtension {
  object EXT {
  }
}

case class GLExtension(name: String) {
  val enableString: String = "#extension " + name + ": enable"
}
