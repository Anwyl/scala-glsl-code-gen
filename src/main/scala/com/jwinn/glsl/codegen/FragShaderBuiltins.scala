package com.jwinn.glsl.codegen

import com.jwinn.glsl.codegen.types.{GLSLBool, GLSLVec2, GLSLVec4}

/**
 * Built-in variables available within fragment shaders
 */
object FragShaderBuiltins {
  // in
  val gl_FragCoord = new GLSLVec4("gl_FragCoord")
  val gl_FrontFacing = new GLSLBool("gl_FrontFacing")
  val gl_PointCoord = new GLSLVec2("gl_PointCoord")
  // out
  val gl_FragColor = new GLSLVec4("gl_FragColor")
}
