package com.jwinn.glsl.codegen

import com.jwinn.glsl.codegen.datastructures.MemberDef
import com.jwinn.glsl.codegen.structures.GLSLBlock
import com.jwinn.glsl.codegen.types.GLSLValue

import scala.collection.mutable.ListBuffer

/**
 * Content of a GLSL function
 * @tparam ReturnType Return type of the function
 */
class FunctionContent[ReturnType <: GLSLValue[ReturnType]] {

  case class Line(content: () ⇒ String)
  private val lineAccumulator: ListBuffer[Line] = new ListBuffer[Line]
  private[codegen] val dependencies: GlobalDependencies = new GlobalDependencies
  private var returnVal: Option[ReturnType] = None

  def definition: String = {
    lineAccumulator.map(GLWhitespace.TAB + _.content()).mkString("") + returnVal.map(GLWhitespace.TAB + "return " + _.glsl + ";" + GLWhitespace.NEWLINE).getOrElse("")
  }

  def addVar (varDef: MemberDef[_]): Unit = lineAccumulator += Line(() ⇒ varDef.definition + ";" + GLWhitespace.NEWLINE)
  def addLine(value: GLSLValue[_]) : Unit = lineAccumulator += Line(() ⇒ value.glsl + ";" + GLWhitespace.NEWLINE)
  def addBlock(value: GLSLBlock)   : Unit = {
    value.context.foreach(nested ⇒ {
      dependencies.addAll(nested.dependencies)
    })
    lineAccumulator += Line(() ⇒ value.definition)
  }

  def returnVal(value: ReturnType): Unit = returnVal = Some(value)
}
