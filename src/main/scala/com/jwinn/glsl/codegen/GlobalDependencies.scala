package com.jwinn.glsl.codegen

import com.jwinn.glsl.codegen.datastructures.VarType.ARRAY
import com.jwinn.glsl.codegen.datastructures.{GlobalDef, VarType}
import com.jwinn.glsl.codegen.structures.Function
import com.jwinn.glsl.codegen.types.Struct

import scala.annotation.tailrec
import scala.collection.mutable

class GlobalDependencies {

  private val extensions  : mutable.ListBuffer[GLExtension] = new mutable.ListBuffer[GLExtension]
  private val functions   : mutable.ListBuffer[Function[_]] = new mutable.ListBuffer[Function[_]]
  private val globalDefs  : mutable.ListBuffer[GlobalDef]   = new mutable.ListBuffer[GlobalDef]
  private val structs     : mutable.ListBuffer[Struct[_]]   = new mutable.ListBuffer[Struct[_]]

  /**
   * Enables the specified GLSL extension
   * @param extension Extension to enable
   */
  def enableExtension(extension: GLExtension): Unit = extensions += extension

  /**
   * Adds the specified helper function to the dependency set.
   * This is done automatically when calling a function
   * @param function Function to add
   */
  def addFunction(function: Function[_]): Unit = {
    addAll(function.context.dependencies)
    functions += function
  }

  /**
   * Adds the specified global variable definition to the dependency set.
   * This is done automatically when the variable is used.
   * @param globalDef Variable to add
   */
  def addGlobalDef(globalDef: GlobalDef): Unit = {
    globalDef.dependentTypes.foreach(addVarTypeDependencies)
    globalDefs += globalDef
  }

  /**
   * Adds the specified struct to the dependency set
   * @param struct Struct to add
   */
  def addStruct(struct: Struct[_]): Unit = {
    struct.block.members.foreach(m ⇒ addVarTypeDependencies(m.varType))
    structs += struct
  }

  /**
   * Add all dependencies of another dependency set to this dependency set
   */
  def addAll(other: GlobalDependencies): Unit = {
    extensions  .addAll(other.extensions)
    functions   .addAll(other.functions)
    globalDefs     .addAll(other.globalDefs)
    structs     .addAll(other.structs)
  }

  /**
   * @return The GLSL code for this dependency set.
   */
  def definition: String = {
    val extensionPart = extensions  .distinct.map(_.enableString     + GLWhitespace.NEWLINE).mkString
    val structPart    = structs     .distinct.map(_.definition + ";" + GLWhitespace.NEWLINE).mkString
    val globalDefPart = globalDefs  .distinct.map(_.definition + ";" + GLWhitespace.NEWLINE).mkString
    val funPart       = functions   .distinct.map(_.definition       + GLWhitespace.NEWLINE).mkString
    Seq(extensionPart, structPart, globalDefPart, funPart).mkString(GLWhitespace.NEWLINE)
  }

  /**
   * Adds dependencies for the VarType
   *
   * @param varType Type to add dependencies of
   */
  @tailrec
  private def addVarTypeDependencies(varType: VarType[_]): Unit = varType match {
    case s: Struct[_] ⇒ addStruct(s)
    case a: ARRAY[_] ⇒ addVarTypeDependencies(a.elementType)
    case _ ⇒
  }
}
