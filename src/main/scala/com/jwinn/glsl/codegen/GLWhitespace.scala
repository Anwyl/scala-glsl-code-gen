package com.jwinn.glsl.codegen

/**
 * Constants for whitespace in GLSL generated code.
 */
object GLWhitespace {
  val NEWLINE = "\n"
  val TAB = "  "
}
