package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.MAT4
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "mat4" typed value
 * @param glsl GLSL which results in the value of type mat4
 */
class GLSLMat4(glsl: String) extends GLSLValue[GLSLMat4](glsl, MAT4) with GLSLMatSquare[GLSLMat4] {
  def this(value: GLSLValue[_]) = this(VarType.MAT4.typeName + "(" + value.glsl + ")")

  def +(other: GLSLFloat): GLSLMat4 = add(other, factory)
  def +(other: GLSLMat4) : GLSLMat4 = add(other, factory)

  def -(other: GLSLFloat): GLSLMat4 = subtract(other, factory)
  def -(other: GLSLMat4) : GLSLMat4 = subtract(other, factory)

  def /(other: GLSLFloat): GLSLMat4 = divide(other, factory)
  def /(other: GLSLMat4) : GLSLMat4 = divide(other, factory)

  def *(other: GLSLFloat): GLSLMat4 = multiply(other, factory)
  def *(other: GLSLMat4) : GLSLMat4 = multiply(other, factory)

  def *(other: GLSLVec4) : GLSLVec4 = multiply(other, new GLSLVec4(_))

  def +=(other: GLSLFloat): GLSLMat4 = plusEqual(other, factory)
  def +=(other: GLSLMat4) : GLSLMat4 = plusEqual(other, factory)

  def -=(other: GLSLFloat): GLSLMat4 = minusEqual(other, factory)
  def -=(other: GLSLMat4) : GLSLMat4 = minusEqual(other, factory)

  def /=(other: GLSLFloat): GLSLMat4 = divideEqual(other, factory)
  def /=(other: GLSLMat4) : GLSLMat4 = divideEqual(other, factory)

  def *=(other: GLSLFloat): GLSLMat4 = timesEuqal(other, factory)
  def *=(other: GLSLMat4) : GLSLMat4 = timesEuqal(other, factory)

  def apply(index: Int): GLSLVec4 = {
    if (index < 0 || index > 3)
      throw new IllegalArgumentException("Attempted to index Mat4[" + index + "]")
    new GLSLVec4(glsl + "[" + index + "]")
  }

  def x = new GLSLVec4(glsl + "[0]")
  def y = new GLSLVec4(glsl + "[1]")
  def z = new GLSLVec4(glsl + "[2]")
  def w = new GLSLVec4(glsl + "[3]")
}