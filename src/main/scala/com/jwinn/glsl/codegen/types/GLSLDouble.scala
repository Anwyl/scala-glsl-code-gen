package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.DOUBLE
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "double" typed value
 * @param glsl GLSL which results in the value of type double
 */
class GLSLDouble(glsl: String) extends GLSLValue[GLSLDouble](glsl, DOUBLE) with GLSLScalar[GLSLDouble] with GenDType[GLSLDouble] {
  def this(value: GLSLValue[_]) = this(VarType.DOUBLE.typeName + "(" + value.glsl + ")")
  def this(value: Double) = this(value.toString)

  def +                  (other: GLSLDouble) : GLSLDouble = add(other, factory)
  def +[T <: GLSLDMat[T]](other: GLSLDMat[T]): T          = add(other, other.factory)
  def +[T <: GLSLDVec[T]](other: GLSLDVec[T]): T          = add(other, other.factory)

  def -                  (other: GLSLDouble) : GLSLDouble = subtract(other, factory)
  def -[T <: GLSLDMat[T]](other: GLSLDMat[T]): T          = subtract(other, other.factory)
  def -[T <: GLSLDVec[T]](other: GLSLDVec[T]): T          = subtract(other, other.factory)

  def /                  (other: GLSLDouble) : GLSLDouble = divide(other, factory)
  def /[T <: GLSLDMat[T]](other: GLSLDMat[T]): T          = divide(other, other.factory)
  def /[T <: GLSLDVec[T]](other: GLSLDVec[T]): T          = divide(other, other.factory)

  def *                  (other: GLSLDouble) : GLSLDouble = multiply(other, factory)
  def *[T <: GLSLDMat[T]](other: GLSLDMat[T]): T          = multiply(other, other.factory)
  def *[T <: GLSLDVec[T]](other: GLSLDVec[T]): T          = multiply(other, other.factory)

  def +=                 (other: GLSLDouble) : GLSLDouble = plusEqual  (other, factory)
  def -=                 (other: GLSLDouble) : GLSLDouble = minusEqual (other, factory)
  def /=                 (other: GLSLDouble) : GLSLDouble = divideEqual(other, factory)
  def *=                 (other: GLSLDouble) : GLSLDouble = timesEuqal (other, factory)

  def unary_- :GLSLDouble = new GLSLDouble("(-" + glsl + ")")

  def > (other: GLSLDouble): GLSLBool = greater       (other)
  def >=(other: GLSLDouble): GLSLBool = greaterOrEqual(other)
  def < (other: GLSLDouble): GLSLBool = lesser        (other)
  def <=(other: GLSLDouble): GLSLBool = lesserOrEqual (other)
  def ==(other: GLSLDouble): GLSLBool = equal         (other)
  def !=(other: GLSLDouble): GLSLBool = notEqual      (other)
}
