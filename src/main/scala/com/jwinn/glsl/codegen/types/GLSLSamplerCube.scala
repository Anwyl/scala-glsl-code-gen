package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.SAMPLER_CUBE
import com.jwinn.glsl.codegen.types.GLSLValue.GLSLSampler

/**
 * GLSL "samplerCube" typed value
 *
 * @param glsl GLSL which results in the value of type samplerCube
 */
class GLSLSamplerCube(glsl: String) extends GLSLValue[GLSLSamplerCube](glsl, SAMPLER_CUBE) with GLSLSampler[GLSLSamplerCube] {
  def this(value: GLSLValue[_]) = this(VarType.SAMPLER_CUBE.typeName + "(" + value.glsl + ")")
}
