package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.VEC2
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "vec2" typed value
 * @param glsl GLSL which results in the value of type vec2
 */
class GLSLVec2(glsl: String) extends GLSLValue[GLSLVec2](glsl, VEC2) with GLSLVec[GLSLVec2] {
  def this(value: GLSLValue[_]) = this(VarType.VEC2.typeName + "(" + value.glsl + ")")

  def +(other: GLSLFloat): GLSLVec2 = add(other, factory)
  def +(other: GLSLVec2) : GLSLVec2 = add(other, factory)

  def -(other: GLSLFloat): GLSLVec2 = subtract(other, factory)
  def -(other: GLSLVec2) : GLSLVec2 = subtract(other, factory)

  def /(other: GLSLFloat): GLSLVec2 = divide(other, factory)
  def /(other: GLSLVec2) : GLSLVec2 = divide(other, factory)

  def *(other: GLSLFloat): GLSLVec2 = multiply(other, factory)
  def *(other: GLSLVec2) : GLSLVec2 = multiply(other, factory)
  def *(other: GLSLMat2) : GLSLVec2 = multiply(other, factory)

  def +=(other: GLSLFloat): GLSLVec2 = plusEqual(other, factory)
  def +=(other: GLSLVec2) : GLSLVec2 = plusEqual(other, factory)

  def -=(other: GLSLFloat): GLSLVec2 = minusEqual(other, factory)
  def -=(other: GLSLVec2) : GLSLVec2 = minusEqual(other, factory)

  def /=(other: GLSLFloat): GLSLVec2 = divideEqual(other, factory)
  def /=(other: GLSLVec2) : GLSLVec2 = divideEqual(other, factory)

  def *=(other: GLSLFloat): GLSLVec2 = timesEuqal(other, factory)
  def *=(other: GLSLVec2) : GLSLVec2 = timesEuqal(other, factory)
  def *=(other: GLSLMat2) : GLSLVec2 = timesEuqal(other, factory)

  def x = new GLSLFloat(glsl + ".x")
  def y = new GLSLFloat(glsl + ".y")
  def xx = new GLSLVec2(glsl + ".xx")
  def xy = new GLSLVec2(glsl + ".xy")
  def yx = new GLSLVec2(glsl + ".yx")
  def yy = new GLSLVec2(glsl + ".yy")
  def xxx = new GLSLVec3(glsl + ".xxx")
  def xxy = new GLSLVec3(glsl + ".xxy")
  def xyx = new GLSLVec3(glsl + ".xyx")
  def xyy = new GLSLVec3(glsl + ".xyy")
  def yxx = new GLSLVec3(glsl + ".yxx")
  def yxy = new GLSLVec3(glsl + ".yxy")
  def yyx = new GLSLVec3(glsl + ".yyx")
  def yyy = new GLSLVec3(glsl + ".yyy")
  def xxxx = new GLSLVec4(glsl + ".xxxx")
  def xxxy = new GLSLVec4(glsl + ".xxxy")
  def xxyx = new GLSLVec4(glsl + ".xxyx")
  def xxyy = new GLSLVec4(glsl + ".xxyy")
  def xyxx = new GLSLVec4(glsl + ".xyxx")
  def xyxy = new GLSLVec4(glsl + ".xyxy")
  def xyyx = new GLSLVec4(glsl + ".xyyx")
  def xyyy = new GLSLVec4(glsl + ".xyyy")
  def yxxx = new GLSLVec4(glsl + ".yxxx")
  def yxxy = new GLSLVec4(glsl + ".yxxy")
  def yxyx = new GLSLVec4(glsl + ".yxyx")
  def yxyy = new GLSLVec4(glsl + ".yxyy")
  def yyxx = new GLSLVec4(glsl + ".yyxx")
  def yyxy = new GLSLVec4(glsl + ".yyxy")
  def yyyx = new GLSLVec4(glsl + ".yyyx")
  def yyyy = new GLSLVec4(glsl + ".yyyy")
}
