package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.MAT2
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "mat2" typed value
 * @param glsl GLSL which results in the value of type mat2
 */
class GLSLMat2(glsl: String) extends GLSLValue[GLSLMat2](glsl, MAT2) with GLSLMatSquare[GLSLMat2] {
  def this(value: GLSLValue[_]) = this(VarType.MAT2.typeName + "(" + value.glsl + ")")

  def +(other: GLSLFloat): GLSLMat2 = add(other, factory)
  def +(other: GLSLMat2) : GLSLMat2 = add(other, factory)

  def -(other: GLSLFloat): GLSLMat2 = subtract(other, factory)
  def -(other: GLSLMat2) : GLSLMat2 = subtract(other, factory)

  def /(other: GLSLFloat): GLSLMat2 = divide(other, factory)
  def /(other: GLSLMat2) : GLSLMat2 = divide(other, factory)

  def *(other: GLSLFloat): GLSLMat2 = multiply(other, factory)
  def *(other: GLSLMat2) : GLSLMat2 = multiply(other, factory)

  def *(other: GLSLVec2) : GLSLVec2 = multiply(other, new GLSLVec2(_))
}