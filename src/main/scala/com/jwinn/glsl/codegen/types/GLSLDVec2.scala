package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.DVEC2
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "dvec2" typed value
 * @param glsl GLSL which results in the value of type dvec2
 */
class GLSLDVec2(glsl: String) extends GLSLValue[GLSLDVec2](glsl, DVEC2) with GLSLDVec[GLSLDVec2] {
  def this(value: GLSLValue[_]) = this(VarType.DVEC2.typeName + "(" + value.glsl + ")")

  def +(other: GLSLDouble): GLSLDVec2 = add(other, factory)
  def +(other: GLSLDVec2) : GLSLDVec2 = add(other, factory)

  def -(other: GLSLDouble): GLSLDVec2 = subtract(other, factory)
  def -(other: GLSLDVec2) : GLSLDVec2 = subtract(other, factory)

  def /(other: GLSLDouble): GLSLDVec2 = divide(other, factory)
  def /(other: GLSLDVec2) : GLSLDVec2 = divide(other, factory)

  def *(other: GLSLDouble): GLSLDVec2 = multiply(other, factory)
  def *(other: GLSLDVec2) : GLSLDVec2 = multiply(other, factory)
  def *(other: GLSLDMat2) : GLSLDVec2 = multiply(other, factory)

  def +=(other: GLSLDouble): GLSLDVec2 = plusEqual(other, factory)
  def +=(other: GLSLDVec2) : GLSLDVec2 = plusEqual(other, factory)

  def -=(other: GLSLDouble): GLSLDVec2 = minusEqual(other, factory)
  def -=(other: GLSLDVec2) : GLSLDVec2 = minusEqual(other, factory)

  def /=(other: GLSLDouble): GLSLDVec2 = divideEqual(other, factory)
  def /=(other: GLSLDVec2) : GLSLDVec2 = divideEqual(other, factory)

  def *=(other: GLSLDouble): GLSLDVec2 = timesEuqal(other, factory)
  def *=(other: GLSLDVec2) : GLSLDVec2 = timesEuqal(other, factory)
  def *=(other: GLSLDMat2) : GLSLDVec2 = timesEuqal(other, factory)

  def unary_- : GLSLDVec2 = factory("(-" + glsl + ")")

  def x = new GLSLDouble(glsl + ".x")
  def y = new GLSLDouble(glsl + ".y")
  def xx = new GLSLDVec2(glsl + ".xx")
  def xy = new GLSLDVec2(glsl + ".xy")
  def yx = new GLSLDVec2(glsl + ".yx")
  def yy = new GLSLDVec2(glsl + ".yy")
  def xxx = new GLSLDVec3(glsl + ".xxx")
  def xxy = new GLSLDVec3(glsl + ".xxy")
  def xyx = new GLSLDVec3(glsl + ".xyx")
  def xyy = new GLSLDVec3(glsl + ".xyy")
  def yxx = new GLSLDVec3(glsl + ".yxx")
  def yxy = new GLSLDVec3(glsl + ".yxy")
  def yyx = new GLSLDVec3(glsl + ".yyx")
  def yyy = new GLSLDVec3(glsl + ".yyy")
  def xxxx = new GLSLDVec4(glsl + ".xxxx")
  def xxxy = new GLSLDVec4(glsl + ".xxxy")
  def xxyx = new GLSLDVec4(glsl + ".xxyx")
  def xxyy = new GLSLDVec4(glsl + ".xxyy")
  def xyxx = new GLSLDVec4(glsl + ".xyxx")
  def xyxy = new GLSLDVec4(glsl + ".xyxy")
  def xyyx = new GLSLDVec4(glsl + ".xyyx")
  def xyyy = new GLSLDVec4(glsl + ".xyyy")
  def yxxx = new GLSLDVec4(glsl + ".yxxx")
  def yxxy = new GLSLDVec4(glsl + ".yxxy")
  def yxyx = new GLSLDVec4(glsl + ".yxyx")
  def yxyy = new GLSLDVec4(glsl + ".yxyy")
  def yyxx = new GLSLDVec4(glsl + ".yyxx")
  def yyxy = new GLSLDVec4(glsl + ".yyxy")
  def yyyx = new GLSLDVec4(glsl + ".yyyx")
  def yyyy = new GLSLDVec4(glsl + ".yyyy")
}
