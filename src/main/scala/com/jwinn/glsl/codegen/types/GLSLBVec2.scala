package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.{BVEC2, DVEC2}
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "dvec2" typed value
 * @param glsl GLSL which results in the value of type dvec2
 */
class GLSLBVec2(glsl: String) extends GLSLValue[GLSLBVec2](glsl, BVEC2) with GLSLBVec[GLSLBVec2] {
  def this(value: GLSLValue[_]) = this(BVEC2.typeName + "(" + value.glsl + ")")

  def &&(other: GLSLBVec2): GLSLBVec2 = op(other, "&&", factory)
  def ||(other: GLSLBVec2): GLSLBVec2 = op(other, "||", factory)

  def x = new GLSLBool(glsl + ".x")
  def y = new GLSLBool(glsl + ".y")
  def xx = new GLSLBVec2(glsl + ".xx")
  def xy = new GLSLBVec2(glsl + ".xy")
  def yx = new GLSLBVec2(glsl + ".yx")
  def yy = new GLSLBVec2(glsl + ".yy")
  def xxx = new GLSLBVec3(glsl + ".xxx")
  def xxy = new GLSLBVec3(glsl + ".xxy")
  def xyx = new GLSLBVec3(glsl + ".xyx")
  def xyy = new GLSLBVec3(glsl + ".xyy")
  def yxx = new GLSLBVec3(glsl + ".yxx")
  def yxy = new GLSLBVec3(glsl + ".yxy")
  def yyx = new GLSLBVec3(glsl + ".yyx")
  def yyy = new GLSLBVec3(glsl + ".yyy")
  def xxxx = new GLSLBVec4(glsl + ".xxxx")
  def xxxy = new GLSLBVec4(glsl + ".xxxy")
  def xxyx = new GLSLBVec4(glsl + ".xxyx")
  def xxyy = new GLSLBVec4(glsl + ".xxyy")
  def xyxx = new GLSLBVec4(glsl + ".xyxx")
  def xyxy = new GLSLBVec4(glsl + ".xyxy")
  def xyyx = new GLSLBVec4(glsl + ".xyyx")
  def xyyy = new GLSLBVec4(glsl + ".xyyy")
  def yxxx = new GLSLBVec4(glsl + ".yxxx")
  def yxxy = new GLSLBVec4(glsl + ".yxxy")
  def yxyx = new GLSLBVec4(glsl + ".yxyx")
  def yxyy = new GLSLBVec4(glsl + ".yxyy")
  def yyxx = new GLSLBVec4(glsl + ".yyxx")
  def yyxy = new GLSLBVec4(glsl + ".yyxy")
  def yyyx = new GLSLBVec4(glsl + ".yyyx")
  def yyyy = new GLSLBVec4(glsl + ".yyyy")
}
