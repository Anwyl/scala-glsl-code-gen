package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.DMAT3
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "dmat3" typed value
 * @param glsl GLSL which results in the value of type dmat3
 */
class GLSLDMat3(glsl: String) extends GLSLValue[GLSLDMat3](glsl, DMAT3) with GLSLDMatSquare[GLSLDMat3]{
  def this(value: GLSLValue[_]) = this(VarType.DMAT3.typeName + "(" + value.glsl + ")")

  def +(other: GLSLDouble): GLSLDMat3 = add(other, factory)
  def +(other: GLSLDMat3) : GLSLDMat3 = add(other, factory)

  def -(other: GLSLDouble): GLSLDMat3 = subtract(other, factory)
  def -(other: GLSLDMat3) : GLSLDMat3 = subtract(other, factory)

  def /(other: GLSLDouble): GLSLDMat3 = divide(other, factory)
  def /(other: GLSLDMat3) : GLSLDMat3 = divide(other, factory)

  def *(other: GLSLDouble): GLSLDMat3 = multiply(other, factory)
  def *(other: GLSLDMat3) : GLSLDMat3 = multiply(other, factory)

  def *(other: GLSLDVec3) : GLSLDVec3 = multiply(other, new GLSLDVec3(_))
}
