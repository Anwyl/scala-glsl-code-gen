package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.DMAT4
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "dmat4" typed value
 * @param glsl GLSL which results in the value of type dmat4
 */
class GLSLDMat4(glsl: String) extends GLSLValue[GLSLDMat4](glsl, DMAT4) with GLSLDMatSquare[GLSLDMat4] {
  def this(value: GLSLValue[_]) = this(VarType.DMAT4.typeName + "(" + value.glsl + ")")

  def +(other: GLSLDouble): GLSLDMat4 = add(other, factory)
  def +(other: GLSLDMat4) : GLSLDMat4 = add(other, factory)

  def -(other: GLSLDouble): GLSLDMat4 = subtract(other, factory)
  def -(other: GLSLDMat4) : GLSLDMat4 = subtract(other, factory)

  def /(other: GLSLDouble): GLSLDMat4 = divide(other, factory)
  def /(other: GLSLDMat4) : GLSLDMat4 = divide(other, factory)

  def *(other: GLSLDouble): GLSLDMat4 = multiply(other, factory)
  def *(other: GLSLDMat4) : GLSLDMat4 = multiply(other, factory)

  def *(other: GLSLDVec4) : GLSLDVec4 = multiply(other, new GLSLDVec4(_))

  def +=(other: GLSLDouble): GLSLDMat4 = plusEqual(other, factory)
  def +=(other: GLSLDMat4) : GLSLDMat4 = plusEqual(other, factory)

  def -=(other: GLSLDouble): GLSLDMat4 = minusEqual(other, factory)
  def -=(other: GLSLDMat4) : GLSLDMat4 = minusEqual(other, factory)

  def /=(other: GLSLDouble): GLSLDMat4 = divideEqual(other, factory)
  def /=(other: GLSLDMat4) : GLSLDMat4 = divideEqual(other, factory)

  def *=(other: GLSLDouble): GLSLDMat4 = timesEuqal(other, factory)
  def *=(other: GLSLDMat4) : GLSLDMat4 = timesEuqal(other, factory)

  def apply(index: Int): GLSLDVec4 = {
    if (index < 0 || index > 3)
      throw new IllegalArgumentException("Attempted to index Mat4[" + index + "]")
    new GLSLDVec4(glsl + "[" + index + "]")
  }

  def x = new GLSLDVec4(glsl + "[0]")
  def y = new GLSLDVec4(glsl + "[1]")
  def z = new GLSLDVec4(glsl + "[2]")
  def w = new GLSLDVec4(glsl + "[3]")
}
