package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.INT
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "int" typed value
 * @param glsl GLSL which results in the value of type int
 */
class GLSLInt(glsl: String) extends GLSLValue[GLSLInt](glsl, INT) with GLSLScalar[GLSLInt] with GenIType[GLSLInt] {
  def this(value: GLSLValue[_]) = this(VarType.INT.typeName + "(" + value.glsl + ")")
  def this(value: Int) = this(value.toString)

  def +   (other: GLSLInt)    : GLSLInt = add(other, factory)
  def +[T <: GLSLIVec[T]](other: GLSLIVec[T]): T       = add(other, other.factory)

  def -   (other: GLSLInt)    : GLSLInt = subtract(other, factory)
  def -[T <: GLSLIVec[T]](other: GLSLIVec[T]): T       = subtract(other, other.factory)

  def /   (other: GLSLInt)    : GLSLInt = divide(other, factory)
  def /[T <: GLSLIVec[T]](other: GLSLIVec[T]): T       = divide(other, other.factory)

  def *   (other: GLSLInt)    : GLSLInt = multiply(other, factory)
  def *[T <: GLSLIVec[T]](other: GLSLIVec[T]): T       = multiply(other, other.factory)

  def +=  (other: GLSLInt)    : GLSLInt = plusEqual  (other, factory)
  def -=  (other: GLSLInt)    : GLSLInt = minusEqual (other, factory)
  def /=  (other: GLSLInt)    : GLSLInt = divideEqual(other, factory)
  def *=  (other: GLSLInt)    : GLSLInt = timesEuqal (other, factory)

  def ++ : GLSLInt = factory("(" + glsl + "++)")
  def -- : GLSLInt = factory("(" + glsl + "--)")

  def %   (other: GLSLInt)    : GLSLInt = op(other, "%",  factory)
  def &   (other: GLSLInt)    : GLSLInt = op(other, "&",  factory)
  def |   (other: GLSLInt)    : GLSLInt = op(other, "|",  factory)
  def ^   (other: GLSLInt)    : GLSLInt = op(other, "^",  factory)
  def >>  (other: GLSLInt)   : GLSLInt = op(other, ">>", factory)
  def >>  (other: GLSLUInt)   : GLSLInt = op(other, ">>", factory)
  def <<  (other: GLSLInt)   : GLSLInt = op(other, "<<", factory)
  def <<  (other: GLSLUInt)   : GLSLInt = op(other, "<<", factory)

  def > (other: GLSLInt): GLSLBool = greater       (other)
  def >=(other: GLSLInt): GLSLBool = greaterOrEqual(other)
  def < (other: GLSLInt): GLSLBool = lesser        (other)
  def <=(other: GLSLInt): GLSLBool = lesserOrEqual (other)
  def ==(other: GLSLInt): GLSLBool = equal         (other)
}
