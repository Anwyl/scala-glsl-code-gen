package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.FLOAT
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "float" typed value
 * @param glsl GLSL which results in the value of type float
 */
class GLSLFloat(glsl: String) extends GLSLValue[GLSLFloat](glsl, FLOAT) with GLSLScalar[GLSLFloat] with GenType[GLSLFloat] {
  def this(value: GLSLValue[_]) = this(VarType.FLOAT.typeName + "(" + value.glsl + ")")
  def this(value: Float) = this(value.toString)

  def +                 (other: GLSLFloat) : GLSLFloat = add(other, factory)
  def +[T <: GLSLMat[T]](other: GLSLMat[T]): T         = add(other, other.factory)
  def +[T <: GLSLVec[T]](other: GLSLVec[T]): T         = add(other, other.factory)

  def -                 (other: GLSLFloat) : GLSLFloat = subtract(other, factory)
  def -[T <: GLSLMat[T]](other: GLSLMat[T]): T         = subtract(other, other.factory)
  def -[T <: GLSLVec[T]](other: GLSLVec[T]): T         = subtract(other, other.factory)

  def /                 (other: GLSLFloat) : GLSLFloat = divide(other, factory)
  def /[T <: GLSLMat[T]](other: GLSLMat[T]): T         = divide(other, other.factory)
  def /[T <: GLSLVec[T]](other: GLSLVec[T]): T         = divide(other, other.factory)

  def *                 (other: GLSLFloat) : GLSLFloat = multiply(other, factory)
  def *[T <: GLSLMat[T]](other: GLSLMat[T]): T         = multiply(other, other.factory)
  def *[T <: GLSLVec[T]](other: GLSLVec[T]): T         = multiply(other, other.factory)

  def +=                 (other: GLSLFloat) : GLSLFloat = plusEqual  (other, factory)
  def -=                 (other: GLSLFloat) : GLSLFloat = minusEqual (other, factory)
  def /=                 (other: GLSLFloat) : GLSLFloat = divideEqual(other, factory)
  def *=                 (other: GLSLFloat) : GLSLFloat = timesEuqal (other, factory)

  def unary_- :GLSLFloat = new GLSLFloat("(-" + glsl + ")")

  def > (other: GLSLFloat): GLSLBool = greater       (other)
  def >=(other: GLSLFloat): GLSLBool = greaterOrEqual(other)
  def < (other: GLSLFloat): GLSLBool = lesser        (other)
  def <=(other: GLSLFloat): GLSLBool = lesserOrEqual (other)
  def ==(other: GLSLFloat): GLSLBool = equal         (other)
}
