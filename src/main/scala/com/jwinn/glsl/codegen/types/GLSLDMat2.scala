package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.DMAT2
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "dmat2" typed value
 * @param glsl GLSL which results in the value of type dmat2
 */
class GLSLDMat2(glsl: String) extends GLSLValue[GLSLDMat2](glsl, DMAT2) with GLSLDMatSquare[GLSLDMat2] {
  def this(value: GLSLValue[_]) = this(VarType.DMAT2.typeName + "(" + value.glsl + ")")

  def +(other: GLSLDouble): GLSLDMat2 = add(other, factory)
  def +(other: GLSLDMat2) : GLSLDMat2 = add(other, factory)

  def -(other: GLSLDouble): GLSLDMat2 = subtract(other, factory)
  def -(other: GLSLDMat2) : GLSLDMat2 = subtract(other, factory)

  def /(other: GLSLDouble): GLSLDMat2 = divide(other, factory)
  def /(other: GLSLDMat2) : GLSLDMat2 = divide(other, factory)

  def *(other: GLSLDouble): GLSLDMat2 = multiply(other, factory)
  def *(other: GLSLDMat2) : GLSLDMat2 = multiply(other, factory)

  def *(other: GLSLDVec2) : GLSLDVec2 = multiply(other, new GLSLDVec2(_))
}