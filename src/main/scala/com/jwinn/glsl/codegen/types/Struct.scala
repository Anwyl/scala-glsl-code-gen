package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.{Block, MemberDef, VarType}

/**
 * GLSL struct type
 * @param name The name of the struct
 * @tparam T The value type of the struct
 */
abstract class Struct[T <: StructInstance[T]](val typeName: String, val block: Block) extends VarType[T] {
  lazy val definition: String = "struct " + typeName + " " + block.definition

  lazy override val alignment: Int = block.alignment
  lazy override val sizeOf: Int = block.sizeOf

  /**
   * Constructor of this struct, ignoring type. Expose with typed 'apply' method in concrete subclasses.
   * @param fields List of values to populate the members of the new struct instance
   * @return A new instance of this struct
   */
  protected def create(fields: Seq[GLSLValue[_]]): T = {
    factory(typeName + "(" + fields.map(_.glsl).mkString(",") + ")")
  }
}

/**
 * GLSL value of type T
 * @param glsl GLSL which results in a value of type T
 * @tparam T Type of the value
 */
abstract class StructInstance[T <: GLSLValue[T]](glsl:String, varType: VarType[T]) extends GLSLValue[T](glsl, varType) {
  protected def ref[U <: GLSLValue[U]](member: MemberDef[U]): U = member.varType.factory(glsl + "." + member.name)
}
