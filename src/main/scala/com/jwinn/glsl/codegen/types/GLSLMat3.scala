package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.MAT3
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "mat3" typed value
 * @param glsl GLSL which results in the value of type mat3
 */
class GLSLMat3(glsl: String) extends GLSLValue[GLSLMat3](glsl, MAT3) with GLSLMatSquare[GLSLMat3] {
  def this(value: GLSLValue[_]) = this(VarType.MAT3.typeName + "(" + value.glsl + ")")

  def +(other: GLSLFloat): GLSLMat3 = add(other, factory)
  def +(other: GLSLMat3) : GLSLMat3 = add(other, factory)

  def -(other: GLSLFloat): GLSLMat3 = subtract(other, factory)
  def -(other: GLSLMat3) : GLSLMat3 = subtract(other, factory)

  def /(other: GLSLFloat): GLSLMat3 = divide(other, factory)
  def /(other: GLSLMat3) : GLSLMat3 = divide(other, factory)

  def *(other: GLSLFloat): GLSLMat3 = multiply(other, factory)
  def *(other: GLSLMat3) : GLSLMat3 = multiply(other, factory)

  def *(other: GLSLVec3) : GLSLVec3 = multiply(other, new GLSLVec3(_))

  def apply(index: Int): GLSLVec3 = {
    if (index < 0 || index > 2)
      throw new IllegalArgumentException("Attempted to index Mat3[" + index + "]")
    new GLSLVec3(glsl + "[" + index + "]")
  }

  def x = new GLSLVec4(glsl + "[0]")
  def y = new GLSLVec4(glsl + "[1]")
  def z = new GLSLVec4(glsl + "[2]")
}