package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.UVEC2
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "uvec2" typed value
 * @param glsl GLSL which results in the value of type uvec2
 */
class GLSLUVec2(glsl: String) extends GLSLValue[GLSLUVec2](glsl, UVEC2) with GLSLUVec[GLSLUVec2] {
  def this(value: GLSLValue[_]) = this(VarType.UVEC2.typeName + "(" + value.glsl + ")")

  def +(other: GLSLUInt): GLSLUVec2 = add(other, factory)
  def +(other: GLSLUVec2) : GLSLUVec2 = add(other, factory)

  def -(other: GLSLUInt): GLSLUVec2 = subtract(other, factory)
  def -(other: GLSLUVec2) : GLSLUVec2 = subtract(other, factory)

  def /(other: GLSLUInt): GLSLUVec2 = divide(other, factory)
  def /(other: GLSLUVec2) : GLSLUVec2 = divide(other, factory)

  def *(other: GLSLUInt): GLSLUVec2 = multiply(other, factory)
  def *(other: GLSLUVec2) : GLSLUVec2 = multiply(other, factory)

  def +=(other: GLSLUInt): GLSLUVec2 = plusEqual(other, factory)
  def +=(other: GLSLUVec2) : GLSLUVec2 = plusEqual(other, factory)

  def -=(other: GLSLUInt): GLSLUVec2 = minusEqual(other, factory)
  def -=(other: GLSLUVec2) : GLSLUVec2 = minusEqual(other, factory)

  def /=(other: GLSLUInt): GLSLUVec2 = divideEqual(other, factory)
  def /=(other: GLSLUVec2) : GLSLUVec2 = divideEqual(other, factory)

  def *=(other: GLSLUInt): GLSLUVec2 = timesEuqal(other, factory)
  def *=(other: GLSLUVec2) : GLSLUVec2 = timesEuqal(other, factory)

  def unary_- : GLSLUVec2 = factory("(-" + glsl + ")")

  def x = new GLSLUInt(glsl + ".x")
  def y = new GLSLUInt(glsl + ".y")
  def xx = new GLSLUVec2(glsl + ".xx")
  def xy = new GLSLUVec2(glsl + ".xy")
  def yx = new GLSLUVec2(glsl + ".yx")
  def yy = new GLSLUVec2(glsl + ".yy")
  def xxx = new GLSLUVec3(glsl + ".xxx")
  def xxy = new GLSLUVec3(glsl + ".xxy")
  def xyx = new GLSLUVec3(glsl + ".xyx")
  def xyy = new GLSLUVec3(glsl + ".xyy")
  def yxx = new GLSLUVec3(glsl + ".yxx")
  def yxy = new GLSLUVec3(glsl + ".yxy")
  def yyx = new GLSLUVec3(glsl + ".yyx")
  def yyy = new GLSLUVec3(glsl + ".yyy")
  def xxxx = new GLSLUVec4(glsl + ".xxxx")
  def xxxy = new GLSLUVec4(glsl + ".xxxy")
  def xxyx = new GLSLUVec4(glsl + ".xxyx")
  def xxyy = new GLSLUVec4(glsl + ".xxyy")
  def xyxx = new GLSLUVec4(glsl + ".xyxx")
  def xyxy = new GLSLUVec4(glsl + ".xyxy")
  def xyyx = new GLSLUVec4(glsl + ".xyyx")
  def xyyy = new GLSLUVec4(glsl + ".xyyy")
  def yxxx = new GLSLUVec4(glsl + ".yxxx")
  def yxxy = new GLSLUVec4(glsl + ".yxxy")
  def yxyx = new GLSLUVec4(glsl + ".yxyx")
  def yxyy = new GLSLUVec4(glsl + ".yxyy")
  def yyxx = new GLSLUVec4(glsl + ".yyxx")
  def yyxy = new GLSLUVec4(glsl + ".yyxy")
  def yyyx = new GLSLUVec4(glsl + ".yyyx")
  def yyyy = new GLSLUVec4(glsl + ".yyyy")
}
