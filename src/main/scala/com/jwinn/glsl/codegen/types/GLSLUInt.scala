package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.UINT
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "uint" typed value
 * @param glsl GLSL which results in the value of type uint
 */
class GLSLUInt(glsl: String) extends GLSLValue[GLSLUInt](glsl, UINT) with GLSLScalar[GLSLUInt] with GenUType[GLSLUInt] {
  def this(value: GLSLValue[_]) = this(VarType.UINT.typeName + "(" + value.glsl + ")")
  def this(value: Int) = this(value.toString)

  def +   (other: GLSLUInt)    : GLSLUInt = add(other, factory)
  def +[T <: GLSLUVec[T]](other: GLSLUVec[T]): T       = add(other, other.factory)

  def -   (other: GLSLUInt)    : GLSLUInt = subtract(other, factory)
  def -[T <: GLSLUVec[T]](other: GLSLUVec[T]): T       = subtract(other, other.factory)

  def /   (other: GLSLUInt)    : GLSLUInt = divide(other, factory)
  def /[T <: GLSLUVec[T]](other: GLSLUVec[T]): T       = divide(other, other.factory)

  def *   (other: GLSLUInt)    : GLSLUInt = multiply(other, factory)
  def *[T <: GLSLUVec[T]](other: GLSLUVec[T]): T       = multiply(other, other.factory)

  def +=  (other: GLSLUInt)    : GLSLUInt = plusEqual  (other, factory)
  def -=  (other: GLSLUInt)    : GLSLUInt = minusEqual (other, factory)
  def /=  (other: GLSLUInt)    : GLSLUInt = divideEqual(other, factory)
  def *=  (other: GLSLUInt)    : GLSLUInt = timesEuqal (other, factory)

  def ++ : GLSLUInt = factory("(" + glsl + "++)")
  def -- : GLSLUInt = factory("(" + glsl + "--)")

  def %   (other: GLSLUInt)    : GLSLUInt = op(other, "%", factory)

  def > (other: GLSLUInt): GLSLBool = greater       (other)
  def >=(other: GLSLUInt): GLSLBool = greaterOrEqual(other)
  def < (other: GLSLUInt): GLSLBool = lesser        (other)
  def <=(other: GLSLUInt): GLSLBool = lesserOrEqual (other)
  def ==(other: GLSLUInt): GLSLBool = equal         (other)
  def !=(other: GLSLUInt): GLSLBool = notEqual      (other)
}
