package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType.VOID

/**
 * GLSL "void" typed value
 * @param glsl GLSL code resulting in void
 */
class GLSLVoid(glsl: String) extends GLSLValue[GLSLVoid](glsl, VOID)
