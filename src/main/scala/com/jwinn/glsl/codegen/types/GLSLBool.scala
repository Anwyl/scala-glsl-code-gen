package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.BOOL
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "bool" typed value
 * @param glsl GLSL code resulting in the boolean value
 */
class GLSLBool(glsl: String) extends GLSLValue[GLSLBool](glsl, BOOL) with GLSLScalar[GLSLBool] {
  def this(value: GLSLValue[_]) = this(VarType.BOOL.typeName + "(" + value.glsl + ")")
  def this(value: Boolean) = this(VarType.BOOL.typeName + "(" + value + ")")

  def &&(other: GLSLBool): GLSLBool = op(other, "&&", factory)
  def ||(other: GLSLBool): GLSLBool = op(other, "||", factory)

  def unary_!(): GLSLBool = factory("(!" + glsl + ")")
}
