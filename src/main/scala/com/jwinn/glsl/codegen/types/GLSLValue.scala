package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.FunctionContent
import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.BOOL

object GLSLValue {

  /**
   * either a GenType, GenDType, GenIType or GenUType
   * @tparam T Type of the value
   */
  trait GenNumericType[T <: GLSLValue[T]] extends GLSLValue[T]

  trait GenType [T <: GLSLValue[T]] extends GenNumericType[T] with GLSLUnitFloat [T]
  trait GenDType[T <: GLSLValue[T]] extends GenNumericType[T] with GLSLUnitDouble[T]
  trait GenIType[T <: GLSLValue[T]] extends GenNumericType[T] with GLSLUnitInt   [T]
  trait GenUType[T <: GLSLValue[T]] extends GenNumericType[T] with GLSLUnitUInt  [T]
  trait GenBType[T <: GLSLValue[T]] extends                        GLSLUnitBool  [T]

  trait GLSLUnitBool  [T <: GLSLValue[T]] extends GLSLValue[T]
  trait GLSLUnitDouble[T <: GLSLValue[T]] extends GLSLValue[T]
  trait GLSLUnitFloat [T <: GLSLValue[T]] extends GLSLValue[T]
  trait GLSLUnitInt   [T <: GLSLValue[T]] extends GLSLValue[T]
  trait GLSLUnitUInt  [T <: GLSLValue[T]] extends GLSLValue[T]
  trait GLSLSampler   [T <: GLSLValue[T]] extends GLSLValue[T]

  trait GLSLScalar[U <: GLSLValue[U]] extends GLSLValue[U]
  trait GLSLVector[U <: GLSLValue[U]] extends GLSLValue[U]
  trait GLSLMatrix[U <: GLSLValue[U]] extends GLSLValue[U]

  trait GLSLDVec[U <: GLSLDVec[U]] extends GLSLVector[U] with GenDType[U]
  trait GLSLVec [U <: GLSLVec [U]] extends GLSLVector[U] with GenType [U]
  trait GLSLIVec[U <: GLSLIVec[U]] extends GLSLVector[U] with GenIType[U]
  trait GLSLUVec[U <: GLSLUVec[U]] extends GLSLVector[U] with GenUType[U]
  trait GLSLBVec[U <: GLSLBVec[U]] extends GLSLVector[U] with GenBType[U]

  trait GLSLDMat[U <: GLSLDMat[U]] extends GLSLMatrix[U] with GLSLUnitDouble[U]
  trait GLSLMat [U <: GLSLMat [U]] extends GLSLMatrix[U] with GLSLUnitFloat [U]

  trait GLSLMatSquare [U <: GLSLMatSquare [U]] extends GLSLMat [U]
  trait GLSLDMatSquare[U <: GLSLDMatSquare[U]] extends GLSLDMat[U]
}

/**
 * GLSL value
 * @param glsl GLSL which results in a value of type T
 * @tparam T Type of the value
 */
abstract class GLSLValue[T <: GLSLValue[T]](val glsl: String, val varType: VarType[T]) {
  def factory(glsl:String): T = varType.factory(glsl)
  def op[OUT](other: GLSLValue[_], op: String, factory: String ⇒ OUT): OUT = {
    factory("(" + glsl + op + other.glsl + ")")
  }
  def multiply      [OUT] (other: GLSLValue[_], factory: String ⇒ OUT): OUT      = op(other, "*",  factory)
  def add           [OUT] (other: GLSLValue[_], factory: String ⇒ OUT): OUT      = op(other, "+",  factory)
  def divide        [OUT] (other: GLSLValue[_], factory: String ⇒ OUT): OUT      = op(other, "/",  factory)
  def subtract      [OUT] (other: GLSLValue[_], factory: String ⇒ OUT): OUT      = op(other, "-",  factory)
  def timesEuqal    [OUT] (other: GLSLValue[_], factory: String ⇒ OUT): OUT      = op(other, "*=",  factory)
  def plusEqual     [OUT] (other: GLSLValue[_], factory: String ⇒ OUT): OUT      = op(other, "+=",  factory)
  def divideEqual   [OUT] (other: GLSLValue[_], factory: String ⇒ OUT): OUT      = op(other, "/=",  factory)
  def minusEqual    [OUT] (other: GLSLValue[_], factory: String ⇒ OUT): OUT      = op(other, "-=",  factory)
  def greater             (other: GLSLValue[_])                       : GLSLBool = op(other, ">",  BOOL.factory)
  def greaterOrEqual      (other: GLSLValue[_])                       : GLSLBool = op(other, ">=", BOOL.factory)
  def lesser              (other: GLSLValue[_])                       : GLSLBool = op(other, "<",  BOOL.factory)
  def lesserOrEqual       (other: GLSLValue[_])                       : GLSLBool = op(other, "<=", BOOL.factory)
  def equal               (other: GLSLValue[_])                       : GLSLBool = op(other, "==", BOOL.factory)
  def notEqual            (other: GLSLValue[_])                       : GLSLBool = op(other, "!=", BOOL.factory)
  def `;`(implicit context: FunctionContent[_]): Unit = {
    context.addLine(this)
  }
  def :=(newVal: T): T = {
    factory(glsl + "=" + newVal.glsl)
  }
}
