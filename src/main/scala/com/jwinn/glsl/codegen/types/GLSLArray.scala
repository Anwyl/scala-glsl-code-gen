package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.ARRAY

/**
 * GLSL array typed value
 *
 * @param glsl GLSL code resulting in the array value
 * @param childType Type of the elements of the array
 * @param declaredLen Length of the array, if explicit
 * @tparam T Type of the elements of the array
 */
class GLSLArray[T <: GLSLValue[T]](glsl: String, childType: VarType[T], val declaredLen: Option[Int]) extends GLSLValue[GLSLArray[T]](glsl, new ARRAY(childType, declaredLen)) {
  def this(glsl:String, childType: VarType[T]) = this(glsl, childType, None)
  def this(glsl:String, childType: VarType[T], length: Int) = this(glsl, childType, Some(length))

  def apply(int: GLSLUInt): T = {
    childType.factory(glsl + "[" + int.glsl + "]")
  }

  def length(): GLSLUInt = new GLSLUInt(glsl + ".length()")
}
