package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.IVEC2
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "ivec2" typed value
 * @param glsl GLSL which results in the value of type ivec2
 */
class GLSLIVec2(glsl: String) extends GLSLValue[GLSLIVec2](glsl, IVEC2) with GLSLIVec[GLSLIVec2] {
  def this(value: GLSLValue[_]) = this(VarType.IVEC2.typeName + "(" + value.glsl + ")")

  def +(other: GLSLInt): GLSLIVec2 = add(other, factory)
  def +(other: GLSLIVec2) : GLSLIVec2 = add(other, factory)

  def -(other: GLSLInt): GLSLIVec2 = subtract(other, factory)
  def -(other: GLSLIVec2) : GLSLIVec2 = subtract(other, factory)

  def /(other: GLSLInt): GLSLIVec2 = divide(other, factory)
  def /(other: GLSLIVec2) : GLSLIVec2 = divide(other, factory)

  def *(other: GLSLInt): GLSLIVec2 = multiply(other, factory)
  def *(other: GLSLIVec2) : GLSLIVec2 = multiply(other, factory)

  def +=(other: GLSLInt): GLSLIVec2 = plusEqual(other, factory)
  def +=(other: GLSLIVec2) : GLSLIVec2 = plusEqual(other, factory)

  def -=(other: GLSLInt): GLSLIVec2 = minusEqual(other, factory)
  def -=(other: GLSLIVec2) : GLSLIVec2 = minusEqual(other, factory)

  def /=(other: GLSLInt): GLSLIVec2 = divideEqual(other, factory)
  def /=(other: GLSLIVec2) : GLSLIVec2 = divideEqual(other, factory)

  def *=(other: GLSLInt): GLSLIVec2 = timesEuqal(other, factory)
  def *=(other: GLSLIVec2) : GLSLIVec2 = timesEuqal(other, factory)

  def unary_- : GLSLIVec2 = factory("(-" + glsl + ")")

  def x = new GLSLInt(glsl + ".x")
  def y = new GLSLInt(glsl + ".y")
  def xx = new GLSLIVec2(glsl + ".xx")
  def xy = new GLSLIVec2(glsl + ".xy")
  def yx = new GLSLIVec2(glsl + ".yx")
  def yy = new GLSLIVec2(glsl + ".yy")
  def xxx = new GLSLIVec3(glsl + ".xxx")
  def xxy = new GLSLIVec3(glsl + ".xxy")
  def xyx = new GLSLIVec3(glsl + ".xyx")
  def xyy = new GLSLIVec3(glsl + ".xyy")
  def yxx = new GLSLIVec3(glsl + ".yxx")
  def yxy = new GLSLIVec3(glsl + ".yxy")
  def yyx = new GLSLIVec3(glsl + ".yyx")
  def yyy = new GLSLIVec3(glsl + ".yyy")
  def xxxx = new GLSLIVec4(glsl + ".xxxx")
  def xxxy = new GLSLIVec4(glsl + ".xxxy")
  def xxyx = new GLSLIVec4(glsl + ".xxyx")
  def xxyy = new GLSLIVec4(glsl + ".xxyy")
  def xyxx = new GLSLIVec4(glsl + ".xyxx")
  def xyxy = new GLSLIVec4(glsl + ".xyxy")
  def xyyx = new GLSLIVec4(glsl + ".xyyx")
  def xyyy = new GLSLIVec4(glsl + ".xyyy")
  def yxxx = new GLSLIVec4(glsl + ".yxxx")
  def yxxy = new GLSLIVec4(glsl + ".yxxy")
  def yxyx = new GLSLIVec4(glsl + ".yxyx")
  def yxyy = new GLSLIVec4(glsl + ".yxyy")
  def yyxx = new GLSLIVec4(glsl + ".yyxx")
  def yyxy = new GLSLIVec4(glsl + ".yyxy")
  def yyyx = new GLSLIVec4(glsl + ".yyyx")
  def yyyy = new GLSLIVec4(glsl + ".yyyy")
}
