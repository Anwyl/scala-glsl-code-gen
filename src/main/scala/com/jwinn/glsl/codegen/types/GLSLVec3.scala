package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.VEC3
import com.jwinn.glsl.codegen.types.GLSLValue._

/**
 * GLSL "vec3" typed value
 * @param glsl GLSL which results in the value of type vec3
 */
class GLSLVec3(glsl: String) extends GLSLValue[GLSLVec3](glsl, VEC3) with GLSLVec[GLSLVec3] {
  def this(value: GLSLValue[_]) = this(VarType.VEC3.typeName + "(" + value.glsl + ")")

  def +(other: GLSLFloat): GLSLVec3 = add(other, factory)
  def +(other: GLSLVec3) : GLSLVec3 = add(other, factory)

  def -(other: GLSLFloat): GLSLVec3 = subtract(other, factory)
  def -(other: GLSLVec3) : GLSLVec3 = subtract(other, factory)

  def /(other: GLSLFloat): GLSLVec3 = divide(other, factory)
  def /(other: GLSLVec3) : GLSLVec3 = divide(other, factory)

  def *(other: GLSLFloat): GLSLVec3 = multiply(other, factory)
  def *(other: GLSLVec3) : GLSLVec3 = multiply(other, factory)
  def *(other: GLSLMat3) : GLSLVec3 = multiply(other, factory)

  def +=(other: GLSLFloat): GLSLVec3 = plusEqual(other, factory)
  def +=(other: GLSLVec3) : GLSLVec3 = plusEqual(other, factory)

  def -=(other: GLSLFloat): GLSLVec3 = minusEqual(other, factory)
  def -=(other: GLSLVec3) : GLSLVec3 = minusEqual(other, factory)

  def /=(other: GLSLFloat): GLSLVec3 = divideEqual(other, factory)
  def /=(other: GLSLVec3) : GLSLVec3 = divideEqual(other, factory)

  def *=(other: GLSLFloat): GLSLVec3 = timesEuqal(other, factory)
  def *=(other: GLSLVec3) : GLSLVec3 = timesEuqal(other, factory)
  def *=(other: GLSLMat3) : GLSLVec3 = timesEuqal(other, factory)

  def unary_- : GLSLVec3 = factory("(-" + glsl + ")")
  
  def x = new GLSLFloat(glsl + ".x")
  def y = new GLSLFloat(glsl + ".y")
  def z = new GLSLFloat(glsl + ".z")
  def xx = new GLSLVec2(glsl + ".xx")
  def xy = new GLSLVec2(glsl + ".xy")
  def xz = new GLSLVec2(glsl + ".xz")
  def yx = new GLSLVec2(glsl + ".yx")
  def yy = new GLSLVec2(glsl + ".yy")
  def yz = new GLSLVec2(glsl + ".yz")
  def zx = new GLSLVec2(glsl + ".zx")
  def zy = new GLSLVec2(glsl + ".zy")
  def zz = new GLSLVec2(glsl + ".zz")
  def xxx = new GLSLVec3(glsl + ".xxx")
  def xxy = new GLSLVec3(glsl + ".xxy")
  def xxz = new GLSLVec3(glsl + ".xxz")
  def xyx = new GLSLVec3(glsl + ".xyx")
  def xyy = new GLSLVec3(glsl + ".xyy")
  def xyz = new GLSLVec3(glsl + ".xyz")
  def xzx = new GLSLVec3(glsl + ".xzx")
  def xzy = new GLSLVec3(glsl + ".xzy")
  def xzz = new GLSLVec3(glsl + ".xzz")
  def yxx = new GLSLVec3(glsl + ".yxx")
  def yxy = new GLSLVec3(glsl + ".yxy")
  def yxz = new GLSLVec3(glsl + ".yxz")
  def yyx = new GLSLVec3(glsl + ".yyx")
  def yyy = new GLSLVec3(glsl + ".yyy")
  def yyz = new GLSLVec3(glsl + ".yyz")
  def yzx = new GLSLVec3(glsl + ".yzx")
  def yzy = new GLSLVec3(glsl + ".yzy")
  def yzz = new GLSLVec3(glsl + ".yzz")
  def zxx = new GLSLVec3(glsl + ".zxx")
  def zxy = new GLSLVec3(glsl + ".zxy")
  def zxz = new GLSLVec3(glsl + ".zxz")
  def zyx = new GLSLVec3(glsl + ".zyx")
  def zyy = new GLSLVec3(glsl + ".zyy")
  def zyz = new GLSLVec3(glsl + ".zyz")
  def zzx = new GLSLVec3(glsl + ".zzx")
  def zzy = new GLSLVec3(glsl + ".zzy")
  def zzz = new GLSLVec3(glsl + ".zzz")
  def xxxx = new GLSLVec4(glsl + ".xxxx")
  def xxxy = new GLSLVec4(glsl + ".xxxy")
  def xxxz = new GLSLVec4(glsl + ".xxxz")
  def xxyx = new GLSLVec4(glsl + ".xxyx")
  def xxyy = new GLSLVec4(glsl + ".xxyy")
  def xxyz = new GLSLVec4(glsl + ".xxyz")
  def xxzx = new GLSLVec4(glsl + ".xxzx")
  def xxzy = new GLSLVec4(glsl + ".xxzy")
  def xxzz = new GLSLVec4(glsl + ".xxzz")
  def xyxx = new GLSLVec4(glsl + ".xyxx")
  def xyxy = new GLSLVec4(glsl + ".xyxy")
  def xyxz = new GLSLVec4(glsl + ".xyxz")
  def xyyx = new GLSLVec4(glsl + ".xyyx")
  def xyyy = new GLSLVec4(glsl + ".xyyy")
  def xyyz = new GLSLVec4(glsl + ".xyyz")
  def xyzx = new GLSLVec4(glsl + ".xyzx")
  def xyzy = new GLSLVec4(glsl + ".xyzy")
  def xyzz = new GLSLVec4(glsl + ".xyzz")
  def xzxx = new GLSLVec4(glsl + ".xzxx")
  def xzxy = new GLSLVec4(glsl + ".xzxy")
  def xzxz = new GLSLVec4(glsl + ".xzxz")
  def xzyx = new GLSLVec4(glsl + ".xzyx")
  def xzyy = new GLSLVec4(glsl + ".xzyy")
  def xzyz = new GLSLVec4(glsl + ".xzyz")
  def xzzx = new GLSLVec4(glsl + ".xzzx")
  def xzzy = new GLSLVec4(glsl + ".xzzy")
  def xzzz = new GLSLVec4(glsl + ".xzzz")
  def yxxx = new GLSLVec4(glsl + ".yxxx")
  def yxxy = new GLSLVec4(glsl + ".yxxy")
  def yxxz = new GLSLVec4(glsl + ".yxxz")
  def yxyx = new GLSLVec4(glsl + ".yxyx")
  def yxyy = new GLSLVec4(glsl + ".yxyy")
  def yxyz = new GLSLVec4(glsl + ".yxyz")
  def yxzx = new GLSLVec4(glsl + ".yxzx")
  def yxzy = new GLSLVec4(glsl + ".yxzy")
  def yxzz = new GLSLVec4(glsl + ".yxzz")
  def yyxx = new GLSLVec4(glsl + ".yyxx")
  def yyxy = new GLSLVec4(glsl + ".yyxy")
  def yyxz = new GLSLVec4(glsl + ".yyxz")
  def yyyx = new GLSLVec4(glsl + ".yyyx")
  def yyyy = new GLSLVec4(glsl + ".yyyy")
  def yyyz = new GLSLVec4(glsl + ".yyyz")
  def yyzx = new GLSLVec4(glsl + ".yyzx")
  def yyzy = new GLSLVec4(glsl + ".yyzy")
  def yyzz = new GLSLVec4(glsl + ".yyzz")
  def yzxx = new GLSLVec4(glsl + ".yzxx")
  def yzxy = new GLSLVec4(glsl + ".yzxy")
  def yzxz = new GLSLVec4(glsl + ".yzxz")
  def yzyx = new GLSLVec4(glsl + ".yzyx")
  def yzyy = new GLSLVec4(glsl + ".yzyy")
  def yzyz = new GLSLVec4(glsl + ".yzyz")
  def yzzx = new GLSLVec4(glsl + ".yzzx")
  def yzzy = new GLSLVec4(glsl + ".yzzy")
  def yzzz = new GLSLVec4(glsl + ".yzzz")
  def zxxx = new GLSLVec4(glsl + ".zxxx")
  def zxxy = new GLSLVec4(glsl + ".zxxy")
  def zxxz = new GLSLVec4(glsl + ".zxxz")
  def zxyx = new GLSLVec4(glsl + ".zxyx")
  def zxyy = new GLSLVec4(glsl + ".zxyy")
  def zxyz = new GLSLVec4(glsl + ".zxyz")
  def zxzx = new GLSLVec4(glsl + ".zxzx")
  def zxzy = new GLSLVec4(glsl + ".zxzy")
  def zxzz = new GLSLVec4(glsl + ".zxzz")
  def zyxx = new GLSLVec4(glsl + ".zyxx")
  def zyxy = new GLSLVec4(glsl + ".zyxy")
  def zyxz = new GLSLVec4(glsl + ".zyxz")
  def zyyx = new GLSLVec4(glsl + ".zyyx")
  def zyyy = new GLSLVec4(glsl + ".zyyy")
  def zyyz = new GLSLVec4(glsl + ".zyyz")
  def zyzx = new GLSLVec4(glsl + ".zyzx")
  def zyzy = new GLSLVec4(glsl + ".zyzy")
  def zyzz = new GLSLVec4(glsl + ".zyzz")
  def zzxx = new GLSLVec4(glsl + ".zzxx")
  def zzxy = new GLSLVec4(glsl + ".zzxy")
  def zzxz = new GLSLVec4(glsl + ".zzxz")
  def zzyx = new GLSLVec4(glsl + ".zzyx")
  def zzyy = new GLSLVec4(glsl + ".zzyy")
  def zzyz = new GLSLVec4(glsl + ".zzyz")
  def zzzx = new GLSLVec4(glsl + ".zzzx")
  def zzzy = new GLSLVec4(glsl + ".zzzy")
  def zzzz = new GLSLVec4(glsl + ".zzzz")

  def r = new GLSLFloat(glsl + ".r")
  def g = new GLSLFloat(glsl + ".g")
  def b = new GLSLFloat(glsl + ".b")
  def rr = new GLSLVec2(glsl + ".rr")
  def rg = new GLSLVec2(glsl + ".rg")
  def rb = new GLSLVec2(glsl + ".rb")
  def gr = new GLSLVec2(glsl + ".gr")
  def gg = new GLSLVec2(glsl + ".gg")
  def gb = new GLSLVec2(glsl + ".gb")
  def br = new GLSLVec2(glsl + ".br")
  def bg = new GLSLVec2(glsl + ".bg")
  def bb = new GLSLVec2(glsl + ".bb")
  def rrr = new GLSLVec3(glsl + ".rrr")
  def rrg = new GLSLVec3(glsl + ".rrg")
  def rrb = new GLSLVec3(glsl + ".rrb")
  def rgr = new GLSLVec3(glsl + ".rgr")
  def rgg = new GLSLVec3(glsl + ".rgg")
  def rgb = new GLSLVec3(glsl + ".rgb")
  def rbr = new GLSLVec3(glsl + ".rbr")
  def rbg = new GLSLVec3(glsl + ".rbg")
  def rbb = new GLSLVec3(glsl + ".rbb")
  def grr = new GLSLVec3(glsl + ".grr")
  def grg = new GLSLVec3(glsl + ".grg")
  def grb = new GLSLVec3(glsl + ".grb")
  def ggr = new GLSLVec3(glsl + ".ggr")
  def ggg = new GLSLVec3(glsl + ".ggg")
  def ggb = new GLSLVec3(glsl + ".ggb")
  def gbr = new GLSLVec3(glsl + ".gbr")
  def gbg = new GLSLVec3(glsl + ".gbg")
  def gbb = new GLSLVec3(glsl + ".gbb")
  def brr = new GLSLVec3(glsl + ".brr")
  def brg = new GLSLVec3(glsl + ".brg")
  def brb = new GLSLVec3(glsl + ".brb")
  def bgr = new GLSLVec3(glsl + ".bgr")
  def bgg = new GLSLVec3(glsl + ".bgg")
  def bgb = new GLSLVec3(glsl + ".bgb")
  def bbr = new GLSLVec3(glsl + ".bbr")
  def bbg = new GLSLVec3(glsl + ".bbg")
  def bbb = new GLSLVec3(glsl + ".bbb")
  def rrrr = new GLSLVec4(glsl + ".rrrr")
  def rrrg = new GLSLVec4(glsl + ".rrrg")
  def rrrb = new GLSLVec4(glsl + ".rrrb")
  def rrgr = new GLSLVec4(glsl + ".rrgr")
  def rrgg = new GLSLVec4(glsl + ".rrgg")
  def rrgb = new GLSLVec4(glsl + ".rrgb")
  def rrbr = new GLSLVec4(glsl + ".rrbr")
  def rrbg = new GLSLVec4(glsl + ".rrbg")
  def rrbb = new GLSLVec4(glsl + ".rrbb")
  def rgrr = new GLSLVec4(glsl + ".rgrr")
  def rgrg = new GLSLVec4(glsl + ".rgrg")
  def rgrb = new GLSLVec4(glsl + ".rgrb")
  def rggr = new GLSLVec4(glsl + ".rggr")
  def rggg = new GLSLVec4(glsl + ".rggg")
  def rggb = new GLSLVec4(glsl + ".rggb")
  def rgbr = new GLSLVec4(glsl + ".rgbr")
  def rgbg = new GLSLVec4(glsl + ".rgbg")
  def rgbb = new GLSLVec4(glsl + ".rgbb")
  def rbrr = new GLSLVec4(glsl + ".rbrr")
  def rbrg = new GLSLVec4(glsl + ".rbrg")
  def rbrb = new GLSLVec4(glsl + ".rbrb")
  def rbgr = new GLSLVec4(glsl + ".rbgr")
  def rbgg = new GLSLVec4(glsl + ".rbgg")
  def rbgb = new GLSLVec4(glsl + ".rbgb")
  def rbbr = new GLSLVec4(glsl + ".rbbr")
  def rbbg = new GLSLVec4(glsl + ".rbbg")
  def rbbb = new GLSLVec4(glsl + ".rbbb")
  def grrr = new GLSLVec4(glsl + ".grrr")
  def grrg = new GLSLVec4(glsl + ".grrg")
  def grrb = new GLSLVec4(glsl + ".grrb")
  def grgr = new GLSLVec4(glsl + ".grgr")
  def grgg = new GLSLVec4(glsl + ".grgg")
  def grgb = new GLSLVec4(glsl + ".grgb")
  def grbr = new GLSLVec4(glsl + ".grbr")
  def grbg = new GLSLVec4(glsl + ".grbg")
  def grbb = new GLSLVec4(glsl + ".grbb")
  def ggrr = new GLSLVec4(glsl + ".ggrr")
  def ggrg = new GLSLVec4(glsl + ".ggrg")
  def ggrb = new GLSLVec4(glsl + ".ggrb")
  def gggr = new GLSLVec4(glsl + ".gggr")
  def gggg = new GLSLVec4(glsl + ".gggg")
  def gggb = new GLSLVec4(glsl + ".gggb")
  def ggbr = new GLSLVec4(glsl + ".ggbr")
  def ggbg = new GLSLVec4(glsl + ".ggbg")
  def ggbb = new GLSLVec4(glsl + ".ggbb")
  def gbrr = new GLSLVec4(glsl + ".gbrr")
  def gbrg = new GLSLVec4(glsl + ".gbrg")
  def gbrb = new GLSLVec4(glsl + ".gbrb")
  def gbgr = new GLSLVec4(glsl + ".gbgr")
  def gbgg = new GLSLVec4(glsl + ".gbgg")
  def gbgb = new GLSLVec4(glsl + ".gbgb")
  def gbbr = new GLSLVec4(glsl + ".gbbr")
  def gbbg = new GLSLVec4(glsl + ".gbbg")
  def gbbb = new GLSLVec4(glsl + ".gbbb")
  def brrr = new GLSLVec4(glsl + ".brrr")
  def brrg = new GLSLVec4(glsl + ".brrg")
  def brrb = new GLSLVec4(glsl + ".brrb")
  def brgr = new GLSLVec4(glsl + ".brgr")
  def brgg = new GLSLVec4(glsl + ".brgg")
  def brgb = new GLSLVec4(glsl + ".brgb")
  def brbr = new GLSLVec4(glsl + ".brbr")
  def brbg = new GLSLVec4(glsl + ".brbg")
  def brbb = new GLSLVec4(glsl + ".brbb")
  def bgrr = new GLSLVec4(glsl + ".bgrr")
  def bgrg = new GLSLVec4(glsl + ".bgrg")
  def bgrb = new GLSLVec4(glsl + ".bgrb")
  def bggr = new GLSLVec4(glsl + ".bggr")
  def bggg = new GLSLVec4(glsl + ".bggg")
  def bggb = new GLSLVec4(glsl + ".bggb")
  def bgbr = new GLSLVec4(glsl + ".bgbr")
  def bgbg = new GLSLVec4(glsl + ".bgbg")
  def bgbb = new GLSLVec4(glsl + ".bgbb")
  def bbrr = new GLSLVec4(glsl + ".bbrr")
  def bbrg = new GLSLVec4(glsl + ".bbrg")
  def bbrb = new GLSLVec4(glsl + ".bbrb")
  def bbgr = new GLSLVec4(glsl + ".bbgr")
  def bbgg = new GLSLVec4(glsl + ".bbgg")
  def bbgb = new GLSLVec4(glsl + ".bbgb")
  def bbbr = new GLSLVec4(glsl + ".bbbr")
  def bbbg = new GLSLVec4(glsl + ".bbbg")
  def bbbb = new GLSLVec4(glsl + ".bbbb")

  def s = new GLSLFloat(glsl + ".s")
  def t = new GLSLFloat(glsl + ".t")
  def p = new GLSLFloat(glsl + ".p")
  def ss = new GLSLVec2(glsl + ".ss")
  def st = new GLSLVec2(glsl + ".st")
  def sp = new GLSLVec2(glsl + ".sp")
  def ts = new GLSLVec2(glsl + ".ts")
  def tt = new GLSLVec2(glsl + ".tt")
  def tp = new GLSLVec2(glsl + ".tp")
  def ps = new GLSLVec2(glsl + ".ps")
  def pt = new GLSLVec2(glsl + ".pt")
  def pp = new GLSLVec2(glsl + ".pp")
  def sss = new GLSLVec3(glsl + ".sss")
  def sst = new GLSLVec3(glsl + ".sst")
  def ssp = new GLSLVec3(glsl + ".ssp")
  def sts = new GLSLVec3(glsl + ".sts")
  def stt = new GLSLVec3(glsl + ".stt")
  def stp = new GLSLVec3(glsl + ".stp")
  def sps = new GLSLVec3(glsl + ".sps")
  def spt = new GLSLVec3(glsl + ".spt")
  def spp = new GLSLVec3(glsl + ".spp")
  def tss = new GLSLVec3(glsl + ".tss")
  def tst = new GLSLVec3(glsl + ".tst")
  def tsp = new GLSLVec3(glsl + ".tsp")
  def tts = new GLSLVec3(glsl + ".tts")
  def ttt = new GLSLVec3(glsl + ".ttt")
  def ttp = new GLSLVec3(glsl + ".ttp")
  def tps = new GLSLVec3(glsl + ".tps")
  def tpt = new GLSLVec3(glsl + ".tpt")
  def tpp = new GLSLVec3(glsl + ".tpp")
  def pss = new GLSLVec3(glsl + ".pss")
  def pst = new GLSLVec3(glsl + ".pst")
  def psp = new GLSLVec3(glsl + ".psp")
  def pts = new GLSLVec3(glsl + ".pts")
  def ptt = new GLSLVec3(glsl + ".ptt")
  def ptp = new GLSLVec3(glsl + ".ptp")
  def pps = new GLSLVec3(glsl + ".pps")
  def ppt = new GLSLVec3(glsl + ".ppt")
  def ppp = new GLSLVec3(glsl + ".ppp")
  def ssss = new GLSLVec4(glsl + ".ssss")
  def ssst = new GLSLVec4(glsl + ".ssst")
  def sssp = new GLSLVec4(glsl + ".sssp")
  def ssts = new GLSLVec4(glsl + ".ssts")
  def sstt = new GLSLVec4(glsl + ".sstt")
  def sstp = new GLSLVec4(glsl + ".sstp")
  def ssps = new GLSLVec4(glsl + ".ssps")
  def sspt = new GLSLVec4(glsl + ".sspt")
  def sspp = new GLSLVec4(glsl + ".sspp")
  def stss = new GLSLVec4(glsl + ".stss")
  def stst = new GLSLVec4(glsl + ".stst")
  def stsp = new GLSLVec4(glsl + ".stsp")
  def stts = new GLSLVec4(glsl + ".stts")
  def sttt = new GLSLVec4(glsl + ".sttt")
  def sttp = new GLSLVec4(glsl + ".sttp")
  def stps = new GLSLVec4(glsl + ".stps")
  def stpt = new GLSLVec4(glsl + ".stpt")
  def stpp = new GLSLVec4(glsl + ".stpp")
  def spss = new GLSLVec4(glsl + ".spss")
  def spst = new GLSLVec4(glsl + ".spst")
  def spsp = new GLSLVec4(glsl + ".spsp")
  def spts = new GLSLVec4(glsl + ".spts")
  def sptt = new GLSLVec4(glsl + ".sptt")
  def sptp = new GLSLVec4(glsl + ".sptp")
  def spps = new GLSLVec4(glsl + ".spps")
  def sppt = new GLSLVec4(glsl + ".sppt")
  def sppp = new GLSLVec4(glsl + ".sppp")
  def tsss = new GLSLVec4(glsl + ".tsss")
  def tsst = new GLSLVec4(glsl + ".tsst")
  def tssp = new GLSLVec4(glsl + ".tssp")
  def tsts = new GLSLVec4(glsl + ".tsts")
  def tstt = new GLSLVec4(glsl + ".tstt")
  def tstp = new GLSLVec4(glsl + ".tstp")
  def tsps = new GLSLVec4(glsl + ".tsps")
  def tspt = new GLSLVec4(glsl + ".tspt")
  def tspp = new GLSLVec4(glsl + ".tspp")
  def ttss = new GLSLVec4(glsl + ".ttss")
  def ttst = new GLSLVec4(glsl + ".ttst")
  def ttsp = new GLSLVec4(glsl + ".ttsp")
  def ttts = new GLSLVec4(glsl + ".ttts")
  def tttt = new GLSLVec4(glsl + ".tttt")
  def tttp = new GLSLVec4(glsl + ".tttp")
  def ttps = new GLSLVec4(glsl + ".ttps")
  def ttpt = new GLSLVec4(glsl + ".ttpt")
  def ttpp = new GLSLVec4(glsl + ".ttpp")
  def tpss = new GLSLVec4(glsl + ".tpss")
  def tpst = new GLSLVec4(glsl + ".tpst")
  def tpsp = new GLSLVec4(glsl + ".tpsp")
  def tpts = new GLSLVec4(glsl + ".tpts")
  def tptt = new GLSLVec4(glsl + ".tptt")
  def tptp = new GLSLVec4(glsl + ".tptp")
  def tpps = new GLSLVec4(glsl + ".tpps")
  def tppt = new GLSLVec4(glsl + ".tppt")
  def tppp = new GLSLVec4(glsl + ".tppp")
  def psss = new GLSLVec4(glsl + ".psss")
  def psst = new GLSLVec4(glsl + ".psst")
  def pssp = new GLSLVec4(glsl + ".pssp")
  def psts = new GLSLVec4(glsl + ".psts")
  def pstt = new GLSLVec4(glsl + ".pstt")
  def pstp = new GLSLVec4(glsl + ".pstp")
  def psps = new GLSLVec4(glsl + ".psps")
  def pspt = new GLSLVec4(glsl + ".pspt")
  def pspp = new GLSLVec4(glsl + ".pspp")
  def ptss = new GLSLVec4(glsl + ".ptss")
  def ptst = new GLSLVec4(glsl + ".ptst")
  def ptsp = new GLSLVec4(glsl + ".ptsp")
  def ptts = new GLSLVec4(glsl + ".ptts")
  def pttt = new GLSLVec4(glsl + ".pttt")
  def pttp = new GLSLVec4(glsl + ".pttp")
  def ptps = new GLSLVec4(glsl + ".ptps")
  def ptpt = new GLSLVec4(glsl + ".ptpt")
  def ptpp = new GLSLVec4(glsl + ".ptpp")
  def ppss = new GLSLVec4(glsl + ".ppss")
  def ppst = new GLSLVec4(glsl + ".ppst")
  def ppsp = new GLSLVec4(glsl + ".ppsp")
  def ppts = new GLSLVec4(glsl + ".ppts")
  def pptt = new GLSLVec4(glsl + ".pptt")
  def pptp = new GLSLVec4(glsl + ".pptp")
  def ppps = new GLSLVec4(glsl + ".ppps")
  def pppt = new GLSLVec4(glsl + ".pppt")
  def pppp = new GLSLVec4(glsl + ".pppp")
}
