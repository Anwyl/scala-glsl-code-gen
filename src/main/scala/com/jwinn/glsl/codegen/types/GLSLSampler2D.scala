package com.jwinn.glsl.codegen.types

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType.SAMPLER2D
import com.jwinn.glsl.codegen.types.GLSLValue.GLSLSampler

/**
 * GLSL "sampler2D" typed value
 *
 * @param glsl GLSL which results in the value of type sampler2D
 */
class GLSLSampler2D(glsl: String) extends GLSLValue[GLSLSampler2D](glsl, SAMPLER2D) with GLSLSampler[GLSLSampler2D] {
  def this(value: GLSLValue[_]) = this(VarType.SAMPLER2D.typeName + "(" + value.glsl + ")")
}
