package com.jwinn.glsl.codegen

import com.jwinn.glsl.codegen.datastructures.VarType
import com.jwinn.glsl.codegen.datastructures.VarType._
import com.jwinn.glsl.codegen.structures._
import com.jwinn.glsl.codegen.types.GLSLValue._
import com.jwinn.glsl.codegen.types._

/**
 * GLSL functions which are built in to GLSL.
 */
object BuiltinFunctions {

  def break(implicit context: FunctionContent[_]): Unit = {
    context.addBlock(GLBreak)
  }
  def continue(implicit context: FunctionContent[_]): Unit = {
    context.addBlock(GLContinue)
  }
  object GLBreak extends GLSLBlock {
    def context: Seq[FunctionContent[_]] = Seq()
    def definition: String = "break;" + GLWhitespace.NEWLINE
  }

  object GLContinue extends GLSLBlock {
    def context: Seq[FunctionContent[_]] = Seq()
    def definition: String = "continue;" + GLWhitespace.NEWLINE
  }

  private def quickFun0[R <: GLSLValue[R]](name: String, returnType: VarType[R]): R = Function0(name, returnType)().applyBuiltin()
  private def quickFun1[A <: GLSLValue[A], R <: GLSLValue[R]](name: String, returnType: VarType[R], a: A): R = Function1(name, returnType, Param("a", a.varType))().applyBuiltin(a)
  private def quickFun2[A <: GLSLValue[A], B <: GLSLValue[B], R <: GLSLValue[R]](name: String, returnType: VarType[R], a: A, b: B): R = Function2(name, returnType, Param("a", a.varType), Param("b", b.varType))().applyBuiltin(a, b)
  private def quickFun3[A <: GLSLValue[A], B <: GLSLValue[B], C <: GLSLValue[C], R <: GLSLValue[R]](name: String, returnType: VarType[R], a: A, b: B, c: C): R = Function3(name, returnType, Param("a", a.varType), Param("b", b.varType), Param("c", c.varType))().applyBuiltin(a, b, c)
  private def quickFun4[A <: GLSLValue[A], B <: GLSLValue[B], C <: GLSLValue[C], D <: GLSLValue[D], R <: GLSLValue[R]](name: String, returnType: VarType[R], a: A, b: B, c: C, d: D): R = Function4(name, returnType, Param("a", a.varType), Param("b", b.varType), Param("c", c.varType), Param("d", d.varType))().applyBuiltin(a, b, c, d)

  private def quickVoid0(name: String)(implicit ctx: FunctionContent[_]): Unit = quickFun0(name, VOID).`;`
  private def quickVoid1[A <: GLSLValue[A]](name: String, a: A)(implicit ctx: FunctionContent[_]): Unit = quickFun1(name, VOID, a).`;`
  private def quickVoid2[A <: GLSLValue[A], B <: GLSLValue[B]](name: String, a: A, b: B)(implicit ctx: FunctionContent[_]): Unit = quickFun2(name, VOID, a, b).`;`
  private def quickVoid3[A <: GLSLValue[A], B <: GLSLValue[B], C <: GLSLValue[C]](name: String, a: A, b: B, c: C)(implicit ctx: FunctionContent[_]): Unit = quickFun3(name, VOID, a, b, c).`;`
  private def quickVoid4[A <: GLSLValue[A], B <: GLSLValue[B], C <: GLSLValue[C], D <: GLSLValue[D]](name: String, a: A, b: B, c: C, d: D)(implicit ctx: FunctionContent[_]): Unit = quickFun4(name, VOID, a, b, c, d).`;`

  def ?:[T <: GLSLValue[T]](test: GLSLBool, ifTrue: T, ifFalse: T): T = ifTrue.factory("((" + test.glsl + ")?(" + ifTrue.glsl + "):(" + ifFalse.glsl + "))")

  def abs[T <: GenType [T]](in: T with GenType [T]) : T = quickFun1("abs", in.varType, in)
  def abs[T <: GenIType[T]](in: T with GenIType[T]) : T = quickFun1("abs", in.varType, in)
  def abs[T <: GenDType[T]](in: T with GenDType[T]) : T = quickFun1("abs", in.varType, in)

  def acos[T <: GenType[T]](in: T): T = quickFun1("acos", in.varType, in)

  def acosh[T <: GenType[T]](in: T): T = quickFun1("acos", in.varType, in)

  def all[T <: GLSLBVec[T]](x: T): GLSLBool = quickFun1("all", BOOL, x)

  def any[T <: GLSLBVec[T]](x: T): GLSLBool = quickFun1("any", BOOL, x)

  def asin[T <: GenType[T]](in: T): T = quickFun1("asin", in.varType, in)

  def asinh[T <: GenType[T]](in: T): T = quickFun1("asinh", in.varType, in)

  def atan[T <: GenType[T]](y: T, x: T): T = quickFun2("atan", y.varType, y, x)
  def atan[T <: GenType[T]](in: T)     : T = quickFun1("atan", in.varType, in)

  def atomicAdd(mem: GLSLInt,  data: GLSLInt) : GLSLInt  = quickFun2("atomicAdd", INT,  mem, data)
  def atomicAdd(mem: GLSLUInt, data: GLSLUInt): GLSLUInt = quickFun2("atomicAdd", UINT, mem, data)

  def atomicAnd(mem: GLSLInt,  data: GLSLInt) : GLSLInt  = quickFun2("atomicAnd", INT,  mem, data)
  def atomicAnd(mem: GLSLUInt, data: GLSLUInt): GLSLUInt = quickFun2("atomicAnd", UINT, mem, data)

  def atomicCompSwap(mem: GLSLInt,  compare: GLSLUInt, data: GLSLUInt): GLSLInt  = quickFun3("atomicCompSwap", INT,  mem, compare, data)
  def atomicCompSwap(mem: GLSLUInt, compare: GLSLUInt, data: GLSLUInt): GLSLUInt = quickFun3("atomicCompSwap", UINT, mem, compare, data)

  def atomicExchange(mem: GLSLInt,  data: GLSLInt) : GLSLInt  = quickFun2("atomicExchange", INT,  mem, data)
  def atomicExchange(mem: GLSLUInt, data: GLSLUInt): GLSLUInt = quickFun2("atomicExchange", UINT, mem, data)

  def atomicMax(mem: GLSLInt,  data: GLSLInt) : GLSLInt  = quickFun2("atomicMax", INT,  mem, data)
  def atomicMax(mem: GLSLUInt, data: GLSLUInt): GLSLUInt = quickFun2("atomicMax", UINT, mem, data)

  def atomicMin(mem: GLSLInt,  data: GLSLInt) : GLSLInt  = quickFun2("atomicMin", INT,  mem, data)
  def atomicMin(mem: GLSLUInt, data: GLSLUInt): GLSLUInt = quickFun2("atomicMin", UINT, mem, data)

  def atomicOr(mem: GLSLInt,  data: GLSLInt) : GLSLInt  = quickFun2("atomicOr", INT,  mem, data)
  def atomicOr(mem: GLSLUInt, data: GLSLUInt): GLSLUInt = quickFun2("atomicOr", UINT, mem, data)

  def atomicXor(mem: GLSLInt,  data: GLSLInt) : GLSLInt  = quickFun2("atomicXor", INT,  mem, data)
  def atomicXor(mem: GLSLUInt, data: GLSLUInt): GLSLUInt = quickFun2("atomicXor", UINT, mem, data)

  // skip atomic buffers

  def barrier(implicit ctx: FunctionContent[_]): Unit = quickVoid0("barrier")

  def bitCount[T <: GenIType[T]](value: T with GenIType[T]): T = quickFun1("bitCount", value.varType, value)
  def bitCount(value: GLSLUInt) : GLSLInt   = quickFun1("bitCount", INT, value)
  def bitCount(value: GLSLUVec2): GLSLIVec2 = quickFun1("bitCount", IVEC2, value)
  def bitCount(value: GLSLUVec3): GLSLIVec3 = quickFun1("bitCount", IVEC3, value)
  def bitCount(value: GLSLUVec4): GLSLIVec4 = quickFun1("bitCount", IVEC4, value)

  def bitfieldExtract[T <: GenIType[T]](value: T with GenIType[T], offset: GLSLInt, bits: GLSLInt): T = quickFun3("bitfieldExtract", value.varType, value, offset, bits)
  def bitfieldExtract[T <: GenUType[T]](value: T with GenUType[T], offset: GLSLInt, bits: GLSLInt): T = quickFun3("bitfieldExtract", value.varType, value, offset, bits)

  def bitfieldInsert[T <: GenIType[T]](base: T with GenIType[T], insert: T with GenIType[T], offset: GLSLInt, bits: GLSLInt): T = quickFun4("bitfieldInsert", base.varType, base, insert, offset, bits)
  def bitfieldInsert[T <: GenUType[T]](base: T with GenUType[T], insert: T with GenUType[T], offset: GLSLInt, bits: GLSLInt): T = quickFun4("bitfieldInsert", base.varType, base, insert, offset, bits)

  def bitfieldReverse[T <: GenIType[T]](value: T with GenIType[T]): T = quickFun1("bitfieldReverse", value.varType, value)
  def bitfieldReverse[T <: GenUType[T]](value: T with GenUType[T]): T = quickFun1("bitfieldReverse", value.varType, value)

  def ceil[T <: GenType [T]](in: T with GenType [T]): T = quickFun1("ceil", in.varType, in)
  def ceil[T <: GenDType[T]](in: T with GenDType[T]): T = quickFun1("ceil", in.varType, in)

  def clamp[T <: GenType[T]] (in: T with GenType[T],  low: GLSLFloat,          high: GLSLFloat)         : T = quickFun3("clamp", in.varType, in, low, high)
  def clamp[T <: GenDType[T]](in: T with GenDType[T], low: GLSLDouble,         high: GLSLDouble)        : T = quickFun3("clamp", in.varType, in, low, high)
  def clamp[T <: GenIType[T]](in: T with GenIType[T], low: GLSLInt,            high: GLSLInt)           : T = quickFun3("clamp", in.varType, in, low, high)
  def clamp[T <: GenUType[T]](in: T with GenUType[T], low: GLSLUInt,           high: GLSLUInt)          : T = quickFun3("clamp", in.varType, in, low, high)
  def clamp[T <: GenNumericType [T]](in: T with GenNumericType [T], low: T with GenNumericType [T] with GLSLVector[T], high: T with GenNumericType [T] with GLSLVector[T]): T = quickFun3[T, T, T, T]("clamp", in.varType, in, low, high)

  def cos[T <: GenType[T]](in: T): T = quickFun1("cos", in.varType, in)

  def cosh[T <: GenType[T]](in: T): T = quickFun1("cosh", in.varType, in)

  def cross(x: GLSLVec3,  y: GLSLVec3) : GLSLVec3  = quickFun2("cross", VEC3, x, y)
  def cross(x: GLSLDVec3, y: GLSLDVec3): GLSLDVec3 = quickFun2("cross", DVEC3, x, y)

  def degrees[T <: GenType[T]](radians: T): T = quickFun1("degrees", radians.varType, radians)

  def determinant[T <: GLSLMatSquare [T]](mat: T with GLSLMatSquare [T]): GLSLFloat  = quickFun1("determinant", FLOAT, mat)
  def determinant[T <: GLSLDMatSquare[T]](mat: T with GLSLDMatSquare[T]): GLSLDouble = quickFun1("determinant", DOUBLE, mat)

  def dFdx      [T <: GenType[T]](p: T): T = quickFun1("dFdx", p.varType, p)
  def dFdy      [T <: GenType[T]](p: T): T = quickFun1("dFdy", p.varType, p)
  def dFdxCoarse[T <: GenType[T]](p: T): T = quickFun1("dFdxCoarse", p.varType, p)
  def dFdyCoarse[T <: GenType[T]](p: T): T = quickFun1("dFdyCoarse", p.varType, p)
  def dFdxFine  [T <: GenType[T]](p: T): T = quickFun1("dFdxFine", p.varType, p)
  def dFdyFine  [T <: GenType[T]](p: T): T = quickFun1("dFdyFine", p.varType, p)

  def distance[T <: GenType[T]] (p0: T with GenType[T],  p1: T with GenType[T]) : GLSLFloat  = quickFun2("distance", FLOAT, p0, p1)
  def distance[T <: GenDType[T]](p0: T with GenDType[T], p1: T with GenDType[T]): GLSLDouble = quickFun2("distance", DOUBLE, p0, p1)

  def dot[T <: GenType[T]] (p0: T with GenType[T],  p1: T with GenType[T]) : GLSLFloat  = quickFun2("dot", FLOAT, p0, p1)
  def dot[T <: GenDType[T]](p0: T with GenDType[T], p1: T with GenDType[T]): GLSLDouble = quickFun2("dot", DOUBLE, p0, p1)

  def EmitStreamVertex(stream: GLSLInt)(implicit ctx: FunctionContent[_]): Unit = quickVoid1("EmitStreamVertex", stream)

  def EmitVertex(implicit ctx: FunctionContent[_]): Unit = quickVoid0("EmitVertex")

  def EndPrimitive(implicit ctx: FunctionContent[_]): Unit = quickVoid0("EndPrimitive")

  def EndStreamPrimitive(stream: GLSLInt)(implicit ctx: FunctionContent[_]): Unit = quickVoid1("EndStreamPrimitive", stream)

  def equal(x: GLSLVec2,  y: GLSLVec2) : GLSLBVec2 = quickFun2("equal", BVEC2, x, y)
  def equal(x: GLSLVec3,  y: GLSLVec3) : GLSLBVec3 = quickFun2("equal", BVEC3, x, y)
  def equal(x: GLSLVec4,  y: GLSLVec4) : GLSLBVec4 = quickFun2("equal", BVEC4, x, y)
  def equal(x: GLSLIVec2, y: GLSLIVec2): GLSLBVec2 = quickFun2("equal", BVEC2, x, y)
  def equal(x: GLSLIVec3, y: GLSLIVec3): GLSLBVec3 = quickFun2("equal", BVEC3, x, y)
  def equal(x: GLSLIVec4, y: GLSLIVec4): GLSLBVec4 = quickFun2("equal", BVEC4, x, y)
  def equal(x: GLSLUVec2, y: GLSLUVec2): GLSLBVec2 = quickFun2("equal", BVEC2, x, y)
  def equal(x: GLSLUVec3, y: GLSLUVec3): GLSLBVec3 = quickFun2("equal", BVEC3, x, y)
  def equal(x: GLSLUVec4, y: GLSLUVec4): GLSLBVec4 = quickFun2("equal", BVEC4, x, y)

  def exp[T <: GenType[T]](x: T): T = quickFun1("exp", x.varType, x)

  def exp2[T <: GenType[T]](x: T): T = quickFun1("exp2", x.varType, x)

  def faceforward[T <: GenType[T]] (N: T with GenType[T],  I: T with GenType[T],  Nref: T with GenType[T]) : T = quickFun3("faceforward", N.varType, N, I, Nref)
  def faceforward[T <: GenDType[T]](N: T with GenDType[T], I: T with GenDType[T], Nref: T with GenDType[T]): T = quickFun3("faceforward", N.varType, N, I, Nref)

  def findLSB[T <: GenIType[T]](value: T with GenIType[T]): T = quickFun1("findLSB", value.varType, value)
  def findLSB[T <: GenUType[T]](value: T with GenUType[T]): T = quickFun1("findLSB", value.varType, value)

  def findMSB[T <: GenIType[T]](value: T with GenIType[T]): T = quickFun1("findMSB", value.varType, value)
  def findMSB[T <: GenUType[T]](value: T with GenUType[T]): T = quickFun1("findMSB", value.varType, value)

  def floatBitsToInt(x: GLSLFloat): GLSLInt   = quickFun1("floatBitsToInt", INT, x)
  def floatBitsToInt(x: GLSLVec2) : GLSLIVec2 = quickFun1("floatBitsToInt", IVEC2, x)
  def floatBitsToInt(x: GLSLVec3) : GLSLIVec3 = quickFun1("floatBitsToInt", IVEC3, x)
  def floatBitsToInt(x: GLSLVec4) : GLSLIVec4 = quickFun1("floatBitsToInt", IVEC4, x)

  def floatBitsToUint(x: GLSLFloat): GLSLUInt  = quickFun1("floatBitsToInt", UINT, x)
  def floatBitsToUint(x: GLSLVec2) : GLSLUVec2 = quickFun1("floatBitsToInt", UVEC2, x)
  def floatBitsToUint(x: GLSLVec3) : GLSLUVec3 = quickFun1("floatBitsToInt", UVEC3, x)
  def floatBitsToUint(x: GLSLVec4) : GLSLUVec4 = quickFun1("floatBitsToInt", UVEC4, x)

  def floor[T <: GenType[T]] (x: T with GenType[T]) : T = quickFun1("floor", x.varType, x)
  def floor[T <: GenDType[T]](x: T with GenDType[T]): T = quickFun1("floor", x.varType, x)

  def fma[T <: GenType[T]] (a: T with GenType [T], b: T with GenType [T], c: T with GenType [T]): T = quickFun3("fma", a.varType, a, b, c)
  def fma[T <: GenDType[T]](a: T with GenDType[T], b: T with GenDType[T], c: T with GenDType[T]): T = quickFun3("fma", a.varType, a, b, c)

  def fract[T <: GenType[T]] (x: T with GenType [T]): T = quickFun1("fract", x.varType, x)
  def fract[T <: GenDType[T]](x: T with GenDType[T]): T = quickFun1("fract", x.varType, x)

  def frexp(x: GLSLFloat,  exp: GLSLInt)  : GLSLFloat  = quickFun2("frexp", FLOAT, x, exp)
  def frexp(x: GLSLVec2,   exp: GLSLIVec2): GLSLVec2   = quickFun2("frexp", VEC2, x, exp)
  def frexp(x: GLSLVec3,   exp: GLSLIVec3): GLSLVec3   = quickFun2("frexp", VEC3, x, exp)
  def frexp(x: GLSLVec4,   exp: GLSLIVec4): GLSLVec4   = quickFun2("frexp", VEC4, x, exp)
  def frexp(x: GLSLDouble, exp: GLSLInt)  : GLSLDouble = quickFun2("frexp", DOUBLE, x, exp)
  def frexp(x: GLSLDVec2,  exp: GLSLIVec2): GLSLDVec2  = quickFun2("frexp", DVEC2, x, exp)
  def frexp(x: GLSLDVec3,  exp: GLSLIVec3): GLSLDVec3  = quickFun2("frexp", DVEC3, x, exp)
  def frexp(x: GLSLDVec4,  exp: GLSLIVec4): GLSLDVec4  = quickFun2("frexp", DVEC4, x, exp)

  def fwidth      [T <: GenType[T]](p: T): T = quickFun1("fwidth", p.varType, p)
  def fwidthCoarse[T <: GenType[T]](p: T): T = quickFun1("fwidthCoarse", p.varType, p)
  def fwidthFine  [T <: GenType[T]](p: T): T = quickFun1("fwidthFine", p.varType, p)

  def greaterThan(x: GLSLVec2,  y: GLSLVec2) : GLSLBVec2 = quickFun2("greaterThan", BVEC2, x, y)
  def greaterThan(x: GLSLVec3,  y: GLSLVec3) : GLSLBVec3 = quickFun2("greaterThan", BVEC3, x, y)
  def greaterThan(x: GLSLVec4,  y: GLSLVec4) : GLSLBVec4 = quickFun2("greaterThan", BVEC4, x, y)
  def greaterThan(x: GLSLIVec2, y: GLSLIVec2): GLSLBVec2 = quickFun2("greaterThan", BVEC2, x, y)
  def greaterThan(x: GLSLIVec3, y: GLSLIVec3): GLSLBVec3 = quickFun2("greaterThan", BVEC3, x, y)
  def greaterThan(x: GLSLIVec4, y: GLSLIVec4): GLSLBVec4 = quickFun2("greaterThan", BVEC4, x, y)
  def greaterThan(x: GLSLUVec2, y: GLSLUVec2): GLSLBVec2 = quickFun2("greaterThan", BVEC2, x, y)
  def greaterThan(x: GLSLUVec3, y: GLSLUVec3): GLSLBVec3 = quickFun2("greaterThan", BVEC3, x, y)
  def greaterThan(x: GLSLUVec4, y: GLSLUVec4): GLSLBVec4 = quickFun2("greaterThan", BVEC4, x, y)

  def greaterThanEqual(x: GLSLVec2,  y: GLSLVec2) : GLSLBVec2 = quickFun2("greaterThanEqual", BVEC2, x, y)
  def greaterThanEqual(x: GLSLVec3,  y: GLSLVec3) : GLSLBVec3 = quickFun2("greaterThanEqual", BVEC3, x, y)
  def greaterThanEqual(x: GLSLVec4,  y: GLSLVec4) : GLSLBVec4 = quickFun2("greaterThanEqual", BVEC4, x, y)
  def greaterThanEqual(x: GLSLIVec2, y: GLSLIVec2): GLSLBVec2 = quickFun2("greaterThanEqual", BVEC2, x, y)
  def greaterThanEqual(x: GLSLIVec3, y: GLSLIVec3): GLSLBVec3 = quickFun2("greaterThanEqual", BVEC3, x, y)
  def greaterThanEqual(x: GLSLIVec4, y: GLSLIVec4): GLSLBVec4 = quickFun2("greaterThanEqual", BVEC4, x, y)
  def greaterThanEqual(x: GLSLUVec2, y: GLSLUVec2): GLSLBVec2 = quickFun2("greaterThanEqual", BVEC2, x, y)
  def greaterThanEqual(x: GLSLUVec3, y: GLSLUVec3): GLSLBVec3 = quickFun2("greaterThanEqual", BVEC3, x, y)
  def greaterThanEqual(x: GLSLUVec4, y: GLSLUVec4): GLSLBVec4 = quickFun2("greaterThanEqual", BVEC4, x, y)

  def groupMemoryBarrier(implicit ctx: FunctionContent[_]): Unit = quickVoid0("groupMemoryBarrier")

  // skip images

  def umulExtended[T <: GenUType[T]](x: T, y: T, msb: T, lsb: T)(implicit ctx: FunctionContent[_]): Unit = quickVoid4("umulExtended", x, y, msb, lsb)
  def imulExtended[T <: GenIType[T]](x: T, y: T, msb: T, lsb: T)(implicit ctx: FunctionContent[_]): Unit = quickVoid4("imulExtended", x, y, msb, lsb)

  def intBitsToFloat (x: GLSLInt)  : GLSLFloat = quickFun1("intBitsToFloat", FLOAT, x)
  def intBitsToFloat (x: GLSLIVec2): GLSLVec2  = quickFun1("intBitsToFloat", VEC2, x)
  def intBitsToFloat (x: GLSLIVec3): GLSLVec3  = quickFun1("intBitsToFloat", VEC3, x)
  def intBitsToFloat (x: GLSLIVec4): GLSLVec4  = quickFun1("intBitsToFloat", VEC4, x)
  def uintBitsToFloat(x: GLSLUInt) : GLSLFloat = quickFun1("uintBitsToFloat", FLOAT, x)
  def uintBitsToFloat(x: GLSLUVec2): GLSLVec2  = quickFun1("uintBitsToFloat", VEC2, x)
  def uintBitsToFloat(x: GLSLUVec3): GLSLVec3  = quickFun1("uintBitsToFloat", VEC3, x)
  def uintBitsToFloat(x: GLSLUVec4): GLSLVec4  = quickFun1("uintBitsToFloat", VEC4, x)

  def interpolateAtCentroid[T <: GenType[T]](interpolant: T): T = quickFun1("interpolateAtCentroid", interpolant.varType, interpolant)

  def interpolateAtOffset[T <: GenType[T]](interpolant: T, offset: GLSLVec2): T = quickFun2("interpolateAtOffset", interpolant.varType, interpolant, offset)

  def interpolateAtSample[T <: GenType[T]](interpolant: T, sample: GLSLInt): T = quickFun2("interpolateAtSample", interpolant.varType, interpolant, sample)

  def inverse[T <: GLSLMatSquare[T]] (mat: T with GLSLMatSquare[T]) : T = quickFun1("inverse", mat.varType, mat)
  def inverse[T <: GLSLDMatSquare[T]](mat: T with GLSLDMatSquare[T]): T = quickFun1("inverse", mat.varType, mat)

  def inversesqrt[T <: GenType[T]] (x: T with GenType[T]) : T = quickFun1("inversesqrt", x.varType, x)
  def inversesqrt[T <: GenDType[T]](x: T with GenDType[T]): T = quickFun1("inversesqrt", x.varType, x)

  def isinf[T <: GenType[T]] (x: T with GenType[T]) : GLSLBool = quickFun1("isinf", BOOL, x)
  def isinf[T <: GenDType[T]](x: T with GenDType[T]): GLSLBool = quickFun1("isinf", BOOL, x)

  def isnan[T <: GenType[T]] (x: T with GenType[T]) : GLSLBool = quickFun1("isnan", BOOL, x)
  def isnan[T <: GenDType[T]](x: T with GenDType[T]): GLSLBool = quickFun1("isnan", BOOL, x)

  def ldexp(x: GLSLFloat,  exp: GLSLInt)  : GLSLFloat  = quickFun2("ldexp", FLOAT,  x, exp)
  def ldexp(x: GLSLVec2,   exp: GLSLIVec2): GLSLVec2   = quickFun2("ldexp", VEC2,   x, exp)
  def ldexp(x: GLSLVec3,   exp: GLSLIVec3): GLSLVec3   = quickFun2("ldexp", VEC3,   x, exp)
  def ldexp(x: GLSLVec4,   exp: GLSLIVec4): GLSLVec4   = quickFun2("ldexp", VEC4,   x, exp)
  def ldexp(x: GLSLDouble, exp: GLSLInt)  : GLSLDouble = quickFun2("ldexp", DOUBLE, x, exp)
  def ldexp(x: GLSLDVec2,  exp: GLSLIVec2): GLSLDVec2  = quickFun2("ldexp", DVEC2,  x, exp)
  def ldexp(x: GLSLDVec3,  exp: GLSLIVec3): GLSLDVec3  = quickFun2("ldexp", DVEC3,  x, exp)
  def ldexp(x: GLSLDVec4,  exp: GLSLIVec4): GLSLDVec4  = quickFun2("ldexp", DVEC4,  x, exp)

  def length[T <: GenType [T]](in: T with GenType [T]): GLSLFloat  = quickFun1("length", FLOAT,  in)
  def length[T <: GenDType[T]](in: T with GenDType[T]): GLSLDouble = quickFun1("length", DOUBLE, in)

  def lessThan(x: GLSLVec2,  y: GLSLVec2) : GLSLBVec2 = quickFun2("lessThan", BVEC2, x, y)
  def lessThan(x: GLSLVec3,  y: GLSLVec3) : GLSLBVec3 = quickFun2("lessThan", BVEC3, x, y)
  def lessThan(x: GLSLVec4,  y: GLSLVec4) : GLSLBVec4 = quickFun2("lessThan", BVEC4, x, y)
  def lessThan(x: GLSLIVec2, y: GLSLIVec2): GLSLBVec2 = quickFun2("lessThan", BVEC2, x, y)
  def lessThan(x: GLSLIVec3, y: GLSLIVec3): GLSLBVec3 = quickFun2("lessThan", BVEC3, x, y)
  def lessThan(x: GLSLIVec4, y: GLSLIVec4): GLSLBVec4 = quickFun2("lessThan", BVEC4, x, y)
  def lessThan(x: GLSLUVec2, y: GLSLUVec2): GLSLBVec2 = quickFun2("lessThan", BVEC2, x, y)
  def lessThan(x: GLSLUVec3, y: GLSLUVec3): GLSLBVec3 = quickFun2("lessThan", BVEC3, x, y)
  def lessThan(x: GLSLUVec4, y: GLSLUVec4): GLSLBVec4 = quickFun2("lessThan", BVEC4, x, y)

  def lessThanEqual(x: GLSLVec2,  y: GLSLVec2) : GLSLBVec2 = quickFun2("lessThanEqual", BVEC2, x, y)
  def lessThanEqual(x: GLSLVec3,  y: GLSLVec3) : GLSLBVec3 = quickFun2("lessThanEqual", BVEC3, x, y)
  def lessThanEqual(x: GLSLVec4,  y: GLSLVec4) : GLSLBVec4 = quickFun2("lessThanEqual", BVEC4, x, y)
  def lessThanEqual(x: GLSLIVec2, y: GLSLIVec2): GLSLBVec2 = quickFun2("lessThanEqual", BVEC2, x, y)
  def lessThanEqual(x: GLSLIVec3, y: GLSLIVec3): GLSLBVec3 = quickFun2("lessThanEqual", BVEC3, x, y)
  def lessThanEqual(x: GLSLIVec4, y: GLSLIVec4): GLSLBVec4 = quickFun2("lessThanEqual", BVEC4, x, y)
  def lessThanEqual(x: GLSLUVec2, y: GLSLUVec2): GLSLBVec2 = quickFun2("lessThanEqual", BVEC2, x, y)
  def lessThanEqual(x: GLSLUVec3, y: GLSLUVec3): GLSLBVec3 = quickFun2("lessThanEqual", BVEC3, x, y)
  def lessThanEqual(x: GLSLUVec4, y: GLSLUVec4): GLSLBVec4 = quickFun2("lessThanEqual", BVEC4, x, y)

  def log[T <: GenType[T]](x: T): T = quickFun1("log", x.varType, x)

  def log2[T <: GenType[T]](x: T): T = quickFun1("log2", x.varType, x)

  def matrixCompMult[T <: GLSLMat[T]] (x: T with GLSLMat[T],  y: T with GLSLMat[T]) : T = quickFun2("matrixCompMult", x.varType, x, y)
  def matrixCompMult[T <: GLSLDMat[T]](x: T with GLSLDMat[T], y: T with GLSLDMat[T]): T = quickFun2("matrixCompMult", x.varType, x, y)

  def max[T <: GenType[T]] (a: T with GenType[T],  b: T with GenType[T]) : T = quickFun2("max", a.varType, a, b)
  def max[T <: GenDType[T]](a: T with GenDType[T], b: T with GenDType[T]): T = quickFun2("max", a.varType, a, b)
  def max[T <: GenUType[T]](a: T with GenUType[T], b: T with GenUType[T]): T = quickFun2("max", a.varType, a, b)
  def max[T <: GenIType[T]](a: T with GenIType[T], b: T with GenIType[T]): T = quickFun2("max", a.varType, a, b)
  def max[T <: GenType[T]] (a: T with GenType[T]  with GLSLVector[T], b: GLSLFloat) : T = quickFun2[T, GLSLFloat,  T]("max", a.varType, a, b)
  def max[T <: GenDType[T]](a: T with GenDType[T] with GLSLVector[T], b: GLSLDouble): T = quickFun2[T, GLSLDouble, T]("max", a.varType, a, b)
  def max[T <: GenUType[T]](a: T with GenUType[T] with GLSLVector[T], b: GLSLUInt)  : T = quickFun2[T, GLSLUInt,   T]("max", a.varType, a, b)
  def max[T <: GenIType[T]](a: T with GenIType[T] with GLSLVector[T], b: GLSLInt)   : T = quickFun2[T, GLSLInt,    T]("max", a.varType, a, b)

  def memoryBarrier(implicit ctx: FunctionContent[_]): Unit = quickVoid0("memoryBarrier")

  def memoryBarrierAtomicCounter(implicit ctx: FunctionContent[_]): Unit = quickVoid0("memoryBarrierAtomicCounter")

  def memoryBarrierBuffer(implicit ctx: FunctionContent[_]): Unit = quickVoid0("memoryBarrierBuffer")

  def memoryBarrierImage(implicit ctx: FunctionContent[_]): Unit = quickVoid0("memoryBarrierImage")

  def memoryBarrierShared(implicit ctx: FunctionContent[_]): Unit = quickVoid0("memoryBarrierShared")

  def min[T <: GenNumericType[T]] (a: T with GenNumericType[T],  b: T with GenNumericType[T]) : T = quickFun2("min", a.varType, a, b)
  def min[T <: GenType[T]] (a: T with GenType[T]  with GLSLVector[T], b: GLSLFloat) : T = quickFun2[T, GLSLFloat,  T]("min", a.varType, a, b)
  def min[T <: GenDType[T]](a: T with GenDType[T] with GLSLVector[T], b: GLSLDouble): T = quickFun2[T, GLSLDouble, T]("min", a.varType, a, b)
  def min[T <: GenUType[T]](a: T with GenUType[T] with GLSLVector[T], b: GLSLUInt)  : T = quickFun2[T, GLSLUInt,   T]("min", a.varType, a, b)
  def min[T <: GenIType[T]](a: T with GenIType[T] with GLSLVector[T], b: GLSLInt)   : T = quickFun2[T, GLSLInt,    T]("min", a.varType, a, b)

  def mix[T <: GenType[T]] (x: T with GenType[T],  y: T with GenType[T],  a: T with GenType[T]) : T = quickFun3("mix", x.varType, x, y, a)
  def mix[T <: GenDType[T]](x: T with GenDType[T], y: T with GenDType[T], a: T with GenDType[T]): T = quickFun3("mix", x.varType, x, y, a)
  def mix[T <: GenBType[T]](x: T with GenBType[T], y: T with GenBType[T], a: T with GenBType[T]): T = quickFun3("mix", x.varType, x, y, a)
  def mix[T <: GenType[T]] (x: T with GenType[T]  with GLSLVector[T], y: T with GenType[T]  with GLSLVector[T], a: GLSLFloat) : T = quickFun3[T, T, GLSLFloat,  T]("mix", x.varType, x, y, a)
  def mix[T <: GenDType[T]](x: T with GenDType[T] with GLSLVector[T], y: T with GenDType[T] with GLSLVector[T], a: GLSLDouble): T = quickFun3[T, T, GLSLDouble, T]("mix", x.varType, x, y, a)
  // mixes of the type mix(GenXType, GenXType, GenBType): GenXType
  def mix(x: GLSLFloat,  y: GLSLFloat,  a: GLSLBool) : GLSLFloat  = quickFun3("mix", FLOAT,  x, y, a)
  def mix(x: GLSLVec2,   y: GLSLVec2,   a: GLSLBVec2): GLSLVec2   = quickFun3("mix", VEC2,   x, y, a)
  def mix(x: GLSLVec3,   y: GLSLVec3,   a: GLSLBVec3): GLSLVec3   = quickFun3("mix", VEC3,   x, y, a)
  def mix(x: GLSLVec4,   y: GLSLVec4,   a: GLSLBVec4): GLSLVec4   = quickFun3("mix", VEC4,   x, y, a)
  def mix(x: GLSLDouble, y: GLSLDouble, a: GLSLBool) : GLSLDouble = quickFun3("mix", DOUBLE, x, y, a)
  def mix(x: GLSLDVec2,  y: GLSLDVec2,  a: GLSLBVec2): GLSLDVec2  = quickFun3("mix", DVEC2,  x, y, a)
  def mix(x: GLSLDVec3,  y: GLSLDVec3,  a: GLSLBVec3): GLSLDVec3  = quickFun3("mix", DVEC3,  x, y, a)
  def mix(x: GLSLDVec4,  y: GLSLDVec4,  a: GLSLBVec4): GLSLDVec4  = quickFun3("mix", DVEC4,  x, y, a)
  def mix(x: GLSLInt,    y: GLSLInt,    a: GLSLBool) : GLSLInt    = quickFun3("mix", INT,    x, y, a)
  def mix(x: GLSLIVec2,  y: GLSLIVec2,  a: GLSLBVec2): GLSLIVec2  = quickFun3("mix", IVEC2,  x, y, a)
  def mix(x: GLSLIVec3,  y: GLSLIVec3,  a: GLSLBVec3): GLSLIVec3  = quickFun3("mix", IVEC3,  x, y, a)
  def mix(x: GLSLIVec4,  y: GLSLIVec4,  a: GLSLBVec4): GLSLIVec4  = quickFun3("mix", IVEC4,  x, y, a)
  def mix(x: GLSLUInt,   y: GLSLUInt,   a: GLSLBool) : GLSLUInt   = quickFun3("mix", UINT,   x, y, a)
  def mix(x: GLSLUVec2,  y: GLSLUVec2,  a: GLSLBVec2): GLSLUVec2  = quickFun3("mix", UVEC2,  x, y, a)
  def mix(x: GLSLUVec3,  y: GLSLUVec3,  a: GLSLBVec3): GLSLUVec3  = quickFun3("mix", UVEC3,  x, y, a)
  def mix(x: GLSLUVec4,  y: GLSLUVec4,  a: GLSLBVec4): GLSLUVec4  = quickFun3("mix", UVEC4,  x, y, a)

  def mod[T <: GenType [T]](a: T with GenType [T], b: GLSLFloat) : T = quickFun2("mod", a.varType, a, b)
  def mod[T <: GenDType[T]](a: T with GenDType[T], b: GLSLDouble): T = quickFun2("mod", a.varType, a, b)
  def mod[T <: GenType [T]](a: T with GenType [T] with GLSLVector[T], b: T with GenType [T] with GLSLVector[T]): T = quickFun2[T, T, T]("mod", a.varType, a, b)
  def mod[T <: GenDType[T]](a: T with GenDType[T] with GLSLVector[T], b: T with GenDType[T] with GLSLVector[T]): T = quickFun2[T, T, T]("mod", a.varType, a, b)

  def modf[T <: GenType[T]] (x: T with GenType[T],  i: T with GenType[T]) : T = quickFun2("modf", x.varType, x, i)
  def modf[T <: GenDType[T]](x: T with GenDType[T], i: T with GenDType[T]): T = quickFun2("modf", x.varType, x, i)

  def noise1[T <: GenType[T]](x: T): GLSLFloat = quickFun1("noise1", FLOAT, x)
  def noise2[T <: GenType[T]](x: T): GLSLVec2  = quickFun1("noise2", VEC2,  x)
  def noise3[T <: GenType[T]](x: T): GLSLVec3  = quickFun1("noise3", VEC3,  x)
  def noise4[T <: GenType[T]](x: T): GLSLVec4  = quickFun1("noise4", VEC4,  x)

  def normalize[T <: GenType[T]] (v: T with GenType[T]) : T = quickFun1("normalize", v.varType, v)
  def normalize[T <: GenDType[T]](v: T with GenDType[T]): T = quickFun1("normalize", v.varType, v)

  def not[T <: GLSLBVec[T]](x: T): T = quickFun1("not", x.varType, x)

  def notEqual(x: GLSLVec2,  y: GLSLVec2) : GLSLBVec2 = quickFun2("notEqual", BVEC2, x, y)
  def notEqual(x: GLSLVec3,  y: GLSLVec3) : GLSLBVec3 = quickFun2("notEqual", BVEC3, x, y)
  def notEqual(x: GLSLVec4,  y: GLSLVec4) : GLSLBVec4 = quickFun2("notEqual", BVEC4, x, y)
  def notEqual(x: GLSLIVec2, y: GLSLIVec2): GLSLBVec2 = quickFun2("notEqual", BVEC2, x, y)
  def notEqual(x: GLSLIVec3, y: GLSLIVec3): GLSLBVec3 = quickFun2("notEqual", BVEC3, x, y)
  def notEqual(x: GLSLIVec4, y: GLSLIVec4): GLSLBVec4 = quickFun2("notEqual", BVEC4, x, y)
  def notEqual(x: GLSLUVec2, y: GLSLUVec2): GLSLBVec2 = quickFun2("notEqual", BVEC2, x, y)
  def notEqual(x: GLSLUVec3, y: GLSLUVec3): GLSLBVec3 = quickFun2("notEqual", BVEC3, x, y)
  def notEqual(x: GLSLUVec4, y: GLSLUVec4): GLSLBVec4 = quickFun2("notEqual", BVEC4, x, y)

  def outerProduct(a: GLSLVec2,  b: GLSLVec2) : GLSLMat2  = quickFun2("outerProduct", MAT2,  a, b)
  def outerProduct(a: GLSLVec3,  b: GLSLVec3) : GLSLMat3  = quickFun2("outerProduct", MAT3,  a, b)
  def outerProduct(a: GLSLVec4,  b: GLSLVec4) : GLSLMat4  = quickFun2("outerProduct", MAT4,  a, b)
  def outerProduct(a: GLSLDVec2, b: GLSLDVec2): GLSLDMat2 = quickFun2("outerProduct", DMAT2, a, b)
  def outerProduct(a: GLSLDVec3, b: GLSLDVec3): GLSLDMat3 = quickFun2("outerProduct", DMAT3, a, b)
  def outerProduct(a: GLSLDVec4, b: GLSLDVec4): GLSLDMat4 = quickFun2("outerProduct", DMAT4, a, b)
// skip non-square outer products

  def packDouble2x32(v: GLSLUVec2): GLSLDouble = quickFun1("packDouble2x32", DOUBLE, v)

  def packHalf2x16(v: GLSLVec2): GLSLUInt = quickFun1("packHalf2x16", UINT, v)

  def packUnorm2x16(v: GLSLVec2): GLSLUInt = quickFun1("packUnorm2x16", UINT, v)

  def packSnorm2x16(v: GLSLVec2): GLSLUInt = quickFun1("packSnorm2x16", UINT, v)

  def packUnorm4x8(v: GLSLVec4): GLSLUInt = quickFun1("packUnorm4x8", UINT, v)

  def packSnorm4x8(v: GLSLVec4): GLSLUInt = quickFun1("packSnorm4x8", UINT, v)

  def pow[T <:GenType[T]](x: T with GenType[T], y: T with GenType[T]): T = quickFun2("pow", x.varType, x, y)

  def radians[T <: GenType[T]](degrees: T): T = quickFun1("radians", degrees.varType, degrees)

  def reflect[T <: GenType [T]](I: T with GenType [T], N: T with GenType[T]) : T = quickFun2("reflect", I.varType, I, N)
  def reflect[T <: GenDType[T]](I: T with GenDType[T], N: T with GenDType[T]): T = quickFun2("reflect", I.varType, I, N)

  def refract[T <: GenType [T]](I: T with GenType [T], N: T with GenType[T],  eta: GLSLFloat): T = quickFun3("refract", I.varType, I, N, eta)
  def refract[T <: GenDType[T]](I: T with GenDType[T], N: T with GenDType[T], eta: GLSLFloat): T = quickFun3("refract", I.varType, I, N, eta)

  def round[T <: GenType [T]](x: T with GenType [T]): T = quickFun1("round", x.varType, x)
  def round[T <: GenDType[T]](x: T with GenDType[T]): T = quickFun1("round", x.varType, x)

  def roundEven[T <: GenType [T]](x: T with GenType [T]): T = quickFun1("roundEven", x.varType, x)
  def roundEven[T <: GenDType[T]](x: T with GenDType[T]): T = quickFun1("roundEven", x.varType, x)

  def sign[T <: GenType [T]](x: T with GenType [T]): T = quickFun1("sign", x.varType, x)
  def sign[T <: GenDType[T]](x: T with GenDType[T]): T = quickFun1("sign", x.varType, x)
  def sign[T <: GenIType[T]](x: T with GenIType[T]): T = quickFun1("sign", x.varType, x)

  def sin[T <: GenType[T]](in: T): T = quickFun1("sin", in.varType, in)

  def sinh[T <: GenType[T]](in: T): T = quickFun1("sinh", in.varType, in)

  def smoothstep[T <: GenType [T]](edge0: T with GenType [T] with GLSLVector[T], edge1: T with GenType [T] with GLSLVector[T], x: T with GenType [T]): T = quickFun3[T, T, T, T]("smoothstep", x.varType, edge0, edge1, x)
  def smoothstep[T <: GenDType[T]](edge0: T with GenDType[T] with GLSLVector[T], edge1: T with GenDType[T] with GLSLVector[T], x: T with GenDType[T]): T = quickFun3[T, T, T, T]("smoothstep", x.varType, edge0, edge1, x)
  def smoothstep[T <: GenType [T]](edge0: GLSLFloat,  edge1: GLSLFloat,  x: T with GenType [T]): T = quickFun3("smoothstep", x.varType, edge0, edge1, x)
  def smoothstep[T <: GenDType[T]](edge0: GLSLDouble, edge1: GLSLDouble, x: T with GenDType[T]): T = quickFun3("smoothstep", x.varType, edge0, edge1, x)

  // sqrt(-0.0) may return NaN. If necessary use sqrt(max(min(0.0, x), x))
  def sqrt[T <: GenType [T]](x: T with GenType [T]): T = quickFun1("sqrt", x.varType, x)
  def sqrt[T <: GenDType[T]](x: T with GenDType[T]): T = quickFun1("sqrt", x.varType, x)

  def step[T <: GenType [T]](edge: T with GenType [T] with GLSLVector[T], x: T with GenType [T]): T = quickFun2[T, T, T]("step", x.varType, edge, x)
  def step[T <: GenDType[T]](edge: T with GenDType[T] with GLSLVector[T], x: T with GenDType[T]): T = quickFun2[T, T, T]("step", x.varType, edge, x)
  def step[T <: GenType [T]](edge: GLSLFloat,  x: T with GenType [T]): T = quickFun2("step", x.varType, edge, x)
  def step[T <: GenDType[T]](edge: GLSLDouble, x: T with GenDType[T]): T = quickFun2("step", x.varType, edge, x)

  def tan[T <: GenType[T]](in: T): T = quickFun1("tan", in.varType, in)

  def tanh[T <: GenType[T]](in: T): T = quickFun1("tanh", in.varType, in)
// skip textel fetch
// skip texture methods

  def texture(sampler: GLSLSampler2D, p: GLSLVec2): GLSLVec4 = quickFun2("texture", VEC4, sampler, p)
  def texture(sampler: GLSLSamplerCube, p: GLSLVec3): GLSLVec4 = quickFun2("texture", VEC4, sampler, p)
  def textureLod(sampler: GLSLSampler2D, p: GLSLVec2, lod: GLSLFloat): GLSLVec4 = quickFun3("textureLod", VEC4, sampler, p, lod)
  def textureLod(sampler: GLSLSamplerCube, p: GLSLVec3, lod: GLSLFloat): GLSLVec4 = quickFun3("textureLod", VEC4, sampler, p, lod)

  def transpose[T <: GLSLMatSquare [T]](m: T with GLSLMatSquare [T]): T = quickFun1("transpose", m.varType, m)
  def transpose[T <: GLSLDMatSquare[T]](m: T with GLSLDMatSquare[T]): T = quickFun1("transpose", m.varType, m)

  def trunc[T <: GenType [T]](in: T with GenType [T]): T = quickFun1("trunc", in.varType, in)
  def trunc[T <: GenDType[T]](in: T with GenDType[T]): T = quickFun1("trunc", in.varType, in)

  def uaddCarry[T <: GenUType[T]](a: T, b: T, carry: T): T = quickFun3("uaddCarry", a.varType, a, b, carry)

  def unpackDouble2x32(d: GLSLDouble): GLSLUVec2 = quickFun1("unpackDouble2x32", UVEC2, d)

  def unpackHalf2x16(v: GLSLUInt): GLSLVec2 = quickFun1("unpackHalf2x16", VEC2, v)

  def unpackUnorm2x16(p: GLSLUInt): GLSLVec2 = quickFun1("unpackUnorm2x16", VEC2, p)

  def unpackSnorm2x16(p: GLSLUInt): GLSLVec2 = quickFun1("unpackSnorm2x16", VEC2, p)

  def unpackUnorm4x8(p: GLSLUInt): GLSLVec4 = quickFun1("unpackUnorm4x8", VEC4, p)

  def unpackSnorm4x8(p: GLSLUInt): GLSLVec4 = quickFun1("unpackSnorm4x8", VEC4, p)

  def usubBorrow[T <: GenUType[T]](x: T, y: T, borrow: T): T = quickFun3("usubBorrow", x.varType, x, y, borrow)

// constructors

  def vec2(x: GLSLFloat): GLSLVec2 = quickFun1("vec2", VEC2, x)
  def vec2(x: GLSLFloat, y: GLSLFloat): GLSLVec2 = quickFun2("vec2", VEC2, x, y)

  def dvec2(x: GLSLDouble): GLSLDVec2 = quickFun1("vec2", DVEC2, x)
  def dvec2(x: GLSLDouble, y: GLSLDouble): GLSLDVec2 = quickFun2("vec2", DVEC2, x, y)

  def uvec2(x: GLSLUInt): GLSLUVec2 = quickFun1("uvec2", UVEC2, x)
  def uvec2(x: GLSLUInt, y: GLSLUInt): GLSLUVec2 = quickFun2("uvec2", UVEC2, x, y)

  def ivec2(x: GLSLInt): GLSLIVec2 = quickFun1("ivec2", IVEC2, x)
  def ivec2(x: GLSLInt, y: GLSLInt): GLSLIVec2 = quickFun2("ivec2", IVEC2, x, y)

  def bvec2(x: GLSLBool): GLSLBVec2 = quickFun1("bvec2", BVEC2, x)
  def bvec2(x: GLSLBool, y: GLSLBool): GLSLBVec2 = quickFun2("bvec2", BVEC2, x, y)

  def vec3(x: GLSLFloat, y: GLSLFloat, z: GLSLFloat): GLSLVec3 = quickFun3("vec3", VEC3, x, y, z)
  def vec3(xy: GLSLVec2, z: GLSLFloat): GLSLVec3 = quickFun2("vec3", VEC3, xy, z)
  def vec3(in: GLSLVec4): GLSLVec3 = quickFun1("vec3", VEC3, in)
  def vec3(in: GLSLVec3): GLSLVec3 = quickFun1("vec3", VEC3, in)
  def vec3(in: GLSLFloat): GLSLVec3 = quickFun1("vec3", VEC3, in)

  def uvec4(x: GLSLUInt): GLSLUVec4 = quickFun1("uvec4", UVEC4, x)
  def uvec4(x: GLSLUInt, y: GLSLUInt, z: GLSLUInt, w: GLSLUInt): GLSLUVec4 = quickFun4("uvec4", UVEC4, x, y, z, w)

  def ivec4(x: GLSLInt): GLSLIVec4 = quickFun1("ivec4", IVEC4, x)
  def ivec4(x: GLSLInt, y: GLSLInt, z: GLSLInt, w: GLSLInt): GLSLIVec4 = quickFun4("ivec4", IVEC4, x, y, z, w)

  def vec4(x: GLSLFloat, y: GLSLFloat, z: GLSLFloat, w: GLSLFloat): GLSLVec4 = quickFun4("vec4", VEC4, x, y, z, w)
  def vec4(xy: GLSLVec2, z: GLSLFloat, w: GLSLFloat): GLSLVec4 = quickFun3("vec4", VEC4, xy, z, w)
  def vec4(xyz: GLSLVec3, w: GLSLFloat): GLSLVec4 = quickFun2("vec4", VEC4, xyz, w)
  def vec4(all: GLSLFloat): GLSLVec4 = quickFun1("vec4", VEC4, all)

  def dvec4(all: GLSLDouble): GLSLDVec4 = quickFun1("dvec4", DVEC4, all)
  def dvec4(xyz: GLSLDVec3, w: GLSLDouble): GLSLDVec4 = quickFun2("dvec4", DVEC4, xyz, w)
  def dvec4(x: GLSLDouble, y: GLSLDouble, z: GLSLDouble, w: GLSLDouble): GLSLDVec4 = quickFun4("dvec4", DVEC4, x, y, z, w)

  def mat2(x: GLSLVec2, y: GLSLVec2): GLSLMat2 = quickFun2("mat2", MAT2, x, y)

  def mat3(allDiagonal: GLSLFloat): GLSLMat3 = quickFun1("mat3", MAT3, allDiagonal)
  def mat3(x: GLSLVec3, y: GLSLVec3, z: GLSLVec3): GLSLMat3 = quickFun3("mat3", MAT3, x, y, z)
  def mat3(mat4: GLSLMat4): GLSLMat3 = quickFun1("mat3", MAT3, mat4)

  def mat4(all: GLSLFloat): GLSLMat4 = quickFun1("mat4", MAT4, all)
  def mat4(x: GLSLVec4, y: GLSLVec4, z: GLSLVec4, w: GLSLVec4): GLSLMat4 = quickFun4("mat4", MAT4, x, y, z, w)

  def dmat4(all: GLSLDouble): GLSLDMat4 = quickFun1("dmat4", DMAT4, all)
}
