package com.jwinn.glsl.codegen.datastructures

import com.jwinn.glsl.codegen.types.GLSLValue
import com.jwinn.glsl.codegen.{AccessType, FunctionContent, TypeQualifier}

/**
 * GLSL Global variable definition
 *
 * @param access Access type of this variable
 * @param member member definition for this variable
 * @tparam ValueType Instance type of this variable
 */
case class VarDef[ValueType <: GLSLValue[ValueType]](access: AccessType, member: MemberDef[ValueType], qualifiers: Seq[TypeQualifier] = Seq()) extends GlobalDef {
  override private[codegen] val definition: String = qualifiers.map(_.glsl + " ").mkString("") + access.glsl + " " + member.definition

  override private[codegen] def dependentTypes = Seq(member.varType)

  def inContext(implicit context: FunctionContent[_]): ValueType = {
    context.dependencies.addGlobalDef(this)
    member.value
  }
}
