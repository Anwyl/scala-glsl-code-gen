package com.jwinn.glsl.codegen.datastructures

import com.jwinn.glsl.codegen.GLWhitespace
import com.jwinn.glsl.codegen.datastructures.VarType.{DOUBLE, INT}

import java.nio.{ByteBuffer, ByteOrder}
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
 * GLSL block
 * Used to define interface blocks and structs
 */
abstract class Block extends HasAlignment {
  /**
   * @return The members of this block
   */
  val members: Seq[MemberDef[_]]

  lazy val definition: String = "{" + GLWhitespace.NEWLINE + members.map(GLWhitespace.TAB + _.definition + ";" + GLWhitespace.NEWLINE).mkString("") + "}"
  lazy val alignment: Int = members.map(_.varType.alignment).max
  lazy val memberSize: Int = members.map(_.varType.sizeOf).sum
  lazy val misalignment: Int = Math.floorMod(-memberSize, alignment)
  lazy val sizeOf: Int = memberSize + misalignment

  protected def alignBuffer(buffer: ByteBuffer, aligned: HasAlignment): Unit = {
    val position = buffer.position()
    buffer.position(position + Math.floorMod(-position, aligned.alignment))
  }

  protected def writeBuffer[U](obj: U, byteLength: Int)(populateObject: (U, ByteBuffer) ⇒ Unit): ByteBuffer = {
    val buffer = ByteBuffer.allocateDirect(byteLength)
    buffer.order(ByteOrder.nativeOrder())
    populateObject(obj, buffer)
    buffer.rewind()
    buffer
  }

  protected def writeBuffer[U](objs: Seq[U], block: Block)(populateObject: (U, ByteBuffer) ⇒ Unit): ByteBuffer = {
    val buffer = ByteBuffer.allocateDirect(objs.length * block.sizeOf)
    buffer.order(ByteOrder.nativeOrder())
    for (obj ← objs) {
      populateObject(obj, buffer)
      alignBuffer(buffer, block)
    }
    buffer.rewind()
    buffer
  }

  protected def write(buffer: ByteBuffer, memberDef: MemberDef[_], writer: ByteBuffer ⇒ Unit): Unit = {
    alignBuffer(buffer, memberDef.varType)
    writer(buffer)
    buffer.position(buffer.position() + memberDef.varType.sizeOf)
  }

  protected def writeDouble(buffer: ByteBuffer, memberDef: MemberDef[_], value: Double): Unit = {
    alignBuffer(buffer, memberDef.varType)
    buffer.asDoubleBuffer().put(value)
    buffer.position(buffer.position() + memberDef.varType.sizeOf)
  }

  protected def writeDoubles(buffer: ByteBuffer, memberDef: MemberDef[_], values: Array[Double]): Unit = {
    alignBuffer(buffer, memberDef.varType)
    buffer.asDoubleBuffer().put(values)
    buffer.position(buffer.position() + memberDef.varType.sizeOf)
  }

  protected def writeInt(buffer: ByteBuffer, memberDef: MemberDef[_], value: Int): Unit = {
    alignBuffer(buffer, memberDef.varType)
    buffer.asIntBuffer().put(value)
    buffer.position(buffer.position() + memberDef.varType.sizeOf)
  }

  protected def writeInts(buffer: ByteBuffer, memberDef: MemberDef[_], values: Seq[Int]): Unit = writeInts(buffer,  memberDef, values.toArray)

  protected def writeInts(buffer: ByteBuffer, memberDef: MemberDef[_], values: Array[Int]): Unit = {
    alignBuffer(buffer, memberDef.varType)
    buffer.asIntBuffer().put(values)
    buffer.position(buffer.position() + memberDef.varType.sizeOf)
  }

  protected def readBuffer[U](numObjs: Int, block: Block, populateBuffer: ByteBuffer ⇒ Unit)(createObj: ByteBuffer ⇒ U): Seq[U] = {
    val buffer = ByteBuffer.allocateDirect(block.sizeOf * numObjs)
    buffer.order(ByteOrder.nativeOrder())
    populateBuffer(buffer)
    (0 until numObjs).map(_ ⇒ {
      val obj = createObj(buffer)
      alignBuffer(buffer, block)
      obj
    })
  }

  protected def readSizedBuffer[U](size: Int, populateBuffer: ByteBuffer ⇒ Unit)(createObj: ByteBuffer ⇒ U): U = {
    val buffer = ByteBuffer.allocateDirect(size)
    buffer.order(ByteOrder.nativeOrder())
    populateBuffer(buffer)
    createObj(buffer)
  }

  protected def readArray[U](buffer: ByteBuffer, length: Int)(fac: ByteBuffer ⇒ U): Seq[U] = {
    (0 until length).map(_ ⇒ fac(buffer))
  }

  protected def readArray[U](buffer: ByteBuffer)(fac: ByteBuffer ⇒ U): Seq[U] = {
    var ret: mutable.ListBuffer[U] = new ListBuffer
    while(buffer.hasRemaining) {
      ret += fac(buffer)
    }
    ret.toSeq
  }

  protected def readDouble(buffer: ByteBuffer, memberDef: MemberDef[_]): Double = {
    alignBuffer(buffer, memberDef.varType)
    buffer.getDouble
  }

  protected def readDoubles(buffer: ByteBuffer, memberDef: MemberDef[_]): Array[Double] = {
    alignBuffer(buffer, memberDef.varType)
    val out = new Array[Double](memberDef.varType.sizeOf / DOUBLE.sizeOf)
    buffer.asDoubleBuffer().get(out)
    buffer.position(buffer.position() + memberDef.varType.sizeOf)
    out
  }

  protected def readInt(buffer: ByteBuffer, memberDef: MemberDef[_]): Int = {
    alignBuffer(buffer, memberDef.varType)
    buffer.getInt
  }

  protected def readInts(buffer: ByteBuffer, memberDef: MemberDef[_]): Array[Int] = {
    alignBuffer(buffer, memberDef.varType)
    val out = new Array[Int](memberDef.varType.sizeOf / INT.sizeOf)
    buffer.asIntBuffer().get(out)
    buffer.position(buffer.position() + memberDef.varType.sizeOf)
    out
  }
}
