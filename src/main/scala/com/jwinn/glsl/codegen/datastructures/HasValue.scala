package com.jwinn.glsl.codegen.datastructures

import com.jwinn.glsl.codegen.types.GLSLValue
import com.jwinn.glsl.codegen.FunctionContent

trait HasValue[ValueType <: GLSLValue[ValueType]] {
  def value(implicit context: FunctionContent[_]): ValueType
  val varType: VarType[ValueType]
}
