package com.jwinn.glsl.codegen.datastructures

import com.jwinn.glsl.codegen._
import com.jwinn.glsl.codegen.types.GLSLValue

object InterfaceBlock {
  def wrapMember[T <: GLSLValue[T]](member: MemberDef[T])(implicit block: InterfaceBlock): HasValue[T] = {
    new HasValue[T] {
      val varType: VarType[T] = member.varType
      def value(implicit context: FunctionContent[_]): T = {
        context.dependencies.addGlobalDef(block)
        member.value
      }
    }
  }
}
class InterfaceBlock(access: AccessType, val blockName: String, block: Block, instanceName: Option[String] = None, typeQualifiers: Seq[TypeQualifier] = Seq()) extends GlobalDef {
  override private[codegen] val definition: String = typeQualifiers.map(_.glsl + " ").mkString + access.glsl + " " + blockName + block.definition + instanceName.getOrElse("")
  override private[codegen] def dependentTypes: Seq[VarType[_]] = block.members.map(_.varType)
}
