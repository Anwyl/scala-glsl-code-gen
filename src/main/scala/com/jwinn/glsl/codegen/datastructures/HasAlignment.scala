package com.jwinn.glsl.codegen.datastructures

trait HasAlignment {
  val alignment: Int
}
