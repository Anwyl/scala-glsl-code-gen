package com.jwinn.glsl.codegen.datastructures

import com.jwinn.glsl.codegen.types.GLSLValue
import com.jwinn.glsl.codegen.FunctionContent

/**
 * Definition of a GLSL global variable
 *
 * @param name         Name of the variable
 * @param varType      Type of the variable
 * @param initialValue Initial value of the variable
 * @tparam ValueType Instance type of the variable
 */
case class MemberDef[ValueType <: GLSLValue[ValueType]](name: String, varType: VarType[ValueType], initialValue: Option[ValueType] = None) extends HasValue[ValueType] {
  def this(name: String, varType: VarType[ValueType], initialValue: ValueType) = this(name, varType, Some(initialValue))

  val definition: String = varType.typeName + " " + name + initialValue.map("=" + _.glsl).getOrElse("")

  override def value(implicit context: FunctionContent[_]): ValueType = varType.factory(name)
}
