package com.jwinn.glsl.codegen.datastructures

import com.jwinn.glsl.codegen.types.GLSLValue
import com.jwinn.glsl.codegen.FunctionContent

case class LocalDef[ValueType <: GLSLValue[ValueType]](name: String, varType: VarType[ValueType], initialValue: Option[ValueType] = None)(implicit context: FunctionContent[_]) extends HasValue[ValueType] {
  def this(name: String, varType: VarType[ValueType], initialValue: ValueType)(implicit context: FunctionContent[_]) = this(name, varType, Some(initialValue))

  private val memberDef = MemberDef(name, varType, initialValue)

  def definition: String = memberDef.definition

  context.addVar(memberDef)

  override def value(implicit context: FunctionContent[_]): ValueType = memberDef.value
}
