package com.jwinn.glsl.codegen.datastructures

import com.jwinn.glsl.codegen.FunctionContent
import com.jwinn.glsl.codegen.types._

/**
 * GLSL variable type.
 * This specifies a type abstractly, without an instance for context.
 * @tparam ValueType The instance type associated with this type.
 */
trait VarType[ValueType <: GLSLValue[ValueType]] extends HasAlignment {

  /**
   * A factory for instances of this type
   */
  val factory: String ⇒ ValueType

  /**
   * Name of this type
   */
  val typeName: String

  /**
   * Size in bytes of instances of this type
   */
  val sizeOf: Int

  /**
   * Byte alignment of this type.
   * All addresses of variables of this type are divisible by alignmentOf.
   */
  val alignment: Int

  /**
   * Creates a member def of this type.
   * @param name Name of the member
   * @param initialValue Initial value of the member
   * @return A new member of this type
   */
  def apply(name: String, initialValue: ValueType)(implicit context: FunctionContent[_]): ValueType = {
    new LocalDef[ValueType](name, this, initialValue).value
  }

  /**
   * Creates a member def of this type.
   * @param name Name of the member
   * @return A new member of this type
   */
  def apply(name: String)(implicit context: FunctionContent[_]): ValueType = {
    new LocalDef[ValueType](name, this).value
  }
}

object VarType {

  object ARRAY {
    def apply[U <: GLSLValue[U]](elementType: VarType[U], length1: Int, length2: Int, length3: Int): VarType[GLSLArray[GLSLArray[GLSLArray[U]]]] = ARRAY(ARRAY(elementType, length1, length2), length3)
    def apply[U <: GLSLValue[U]](elementType: VarType[U], length1: Int, length2: Int): VarType[GLSLArray[GLSLArray[U]]] = ARRAY(ARRAY(elementType, length1), length2)
    def apply[U <: GLSLValue[U]](elementType: VarType[U], length: Int) = new ARRAY(elementType, length)
    def apply[U <: GLSLValue[U]](elementType: VarType[U]) = new ARRAY(elementType)
  }
  class ARRAY[U <: GLSLValue[U]](val elementType: VarType[U], val length: Option[Int]) extends VarType[GLSLArray[U]] {
    def this(elementType: VarType[U], length: Int) = this(elementType, Some(length))
    def this(elementType: VarType[U]) = this(elementType, None)
    val typeName: String = elementType.typeName + "[" + length.getOrElse("") + "]"
    val factory = new GLSLArray[U](_, elementType, length)
    lazy val sizeOf: Int = elementType.sizeOf * length.getOrElse(throw new IllegalStateException("Array has unbounded length: " + typeName))
    val alignment: Int = elementType match {
      case _: VEC3.type ⇒ VEC3.sizeOf
      case _: DVEC3.type ⇒ DVEC3.sizeOf
      case _: UVEC3.type ⇒ UVEC3.sizeOf
      case _ ⇒ elementType.alignment
    }
  }

  case object SAMPLER_CUBE extends VarType[GLSLSamplerCube] {
    val typeName: String = "samplerCube"
    val factory: String ⇒ GLSLSamplerCube = new GLSLSamplerCube(_)
    val sizeOf: Int = 4
    val alignment: Int = sizeOf
  }

  case object SAMPLER2D extends VarType[GLSLSampler2D] {
    val typeName: String = "sampler2D"
    val factory: String ⇒ GLSLSampler2D = new GLSLSampler2D(_)
    val sizeOf: Int = 4
    val alignment: Int = sizeOf
  }

  case object BOOL extends VarType[GLSLBool] {
    val typeName = "bool"
    val factory: String ⇒ GLSLBool = new GLSLBool(_)
    val sizeOf: Int = 4
    val alignment: Int = sizeOf
  }

  case object BVEC2 extends VarType[GLSLBVec2] {
    val typeName = "bvec2"
    val factory = new GLSLBVec2(_)
    val sizeOf: Int = BOOL.sizeOf * 2
    val alignment: Int = sizeOf
  }

  case object BVEC3 extends VarType[GLSLBVec3] {
    val typeName = "bvec3"
    val factory = new GLSLBVec3(_)
    val sizeOf: Int = BOOL.sizeOf * 3
    val alignment: Int = BVEC4.alignment
  }

  case object BVEC4 extends VarType[GLSLBVec4] {
    val typeName = "bvec4"
    val factory = new GLSLBVec4(_)
    val sizeOf: Int = BOOL.sizeOf * 4
    val alignment: Int = sizeOf
  }

  case object DMAT2 extends VarType[GLSLDMat2] {
    val typeName = "dmat2"
    val factory = new GLSLDMat2(_)
    val sizeOf: Int = DOUBLE.sizeOf * 2 * 2
    val alignment: Int = sizeOf
  }

  case object DMAT3 extends VarType[GLSLDMat3] {
    val typeName = "dmat3"
    val factory = new GLSLDMat3(_)
    val sizeOf: Int = DOUBLE.sizeOf * 3 * 3
    val alignment: Int = DVEC4.alignment * 3
  }

  case object DMAT4 extends VarType[GLSLDMat4] {
    val typeName = "dmat4"
    val factory = new GLSLDMat4(_)
    val sizeOf: Int = DOUBLE.sizeOf * 4 * 4
    val alignment: Int = sizeOf
  }

  case object DOUBLE extends VarType[GLSLDouble] {
    val typeName = "double"
    val factory = new GLSLDouble(_)
    val sizeOf: Int = 8
    val alignment: Int = sizeOf
  }

  case object DVEC2 extends VarType[GLSLDVec2] {
    val typeName = "dvec2"
    val factory = new GLSLDVec2(_)
    val sizeOf: Int = DOUBLE.sizeOf * 2
    val alignment: Int = sizeOf
  }

  case object DVEC3 extends VarType[GLSLDVec3] {
    val typeName = "dvec3"
    val factory = new GLSLDVec3(_)
    val sizeOf: Int = DOUBLE.sizeOf * 3
    val alignment: Int = DVEC4.alignment
  }

  case object DVEC4 extends VarType[GLSLDVec4] {
    val typeName = "dvec4"
    val factory = new GLSLDVec4(_)
    val sizeOf: Int = DOUBLE.sizeOf * 4
    val alignment: Int = sizeOf
  }

  case object FLOAT extends VarType[GLSLFloat] {
    val typeName = "float"
    val factory = new GLSLFloat(_)
    val sizeOf: Int = 4
    val alignment: Int = sizeOf
  }

  case object INT extends VarType[GLSLInt] {
    val typeName = "int"
    val factory = new GLSLInt(_)
    val sizeOf: Int = 4
    val alignment: Int = sizeOf
  }

  case object IVEC2 extends VarType[GLSLIVec2] {
    val typeName = "ivec2"
    val factory = new GLSLIVec2(_)
    val sizeOf: Int = INT.sizeOf * 2
    val alignment: Int = sizeOf
  }

  case object IVEC3 extends VarType[GLSLIVec3] {
    val typeName = "ivec3"
    val factory = new GLSLIVec3(_)
    val sizeOf: Int = INT.sizeOf * 3
    val alignment: Int = IVEC4.alignment
  }

  case object IVEC4 extends VarType[GLSLIVec4] {
    val typeName = "ivec4"
    val factory = new GLSLIVec4(_)
    val sizeOf: Int = INT.sizeOf * 4
    val alignment: Int = sizeOf
  }

  case object UINT extends VarType[GLSLUInt] {
    val typeName = "uint"
    val factory = new GLSLUInt(_)
    val sizeOf: Int = 4
    val alignment: Int = sizeOf
  }

  case object UVEC2 extends VarType[GLSLUVec2] {
    val typeName = "uvec2"
    val factory = new GLSLUVec2(_)
    val sizeOf: Int = UINT.sizeOf * 2
    val alignment: Int = sizeOf
  }

  case object UVEC3 extends VarType[GLSLUVec3] {
    val typeName = "uvec3"
    val factory = new GLSLUVec3(_)
    val sizeOf: Int = UINT.sizeOf * 3
    val alignment: Int = UVEC4.alignment
  }

  case object UVEC4 extends VarType[GLSLUVec4] {
    val typeName = "uvec4"
    val factory = new GLSLUVec4(_)
    val sizeOf: Int = UINT.sizeOf * 4
    val alignment: Int = sizeOf
  }

  case object MAT2 extends VarType[GLSLMat2] {
    val typeName = "mat2"
    val factory = new GLSLMat2(_)
    val sizeOf: Int = FLOAT.sizeOf * 2 * 2
    val alignment: Int = sizeOf
  }

  case object MAT3 extends VarType[GLSLMat3] {
    val typeName = "mat3"
    val factory = new GLSLMat3(_)
    val sizeOf: Int = FLOAT.sizeOf * 3 * 3
    val alignment: Int = VEC4.alignment * 3
  }

  case object MAT4 extends VarType[GLSLMat4] {
    val typeName = "mat4"
    val factory = new GLSLMat4(_)
    val sizeOf: Int = FLOAT.sizeOf * 4 * 4
    val alignment: Int = sizeOf
  }

  case object VEC2 extends VarType[GLSLVec2] {
    val typeName = "vec2"
    val factory = new GLSLVec2(_)
    val sizeOf: Int = FLOAT.sizeOf * 2
    val alignment: Int = sizeOf
  }

  case object VEC3 extends VarType[GLSLVec3] {
    val typeName = "vec3"
    val factory = new GLSLVec3(_)
    val sizeOf: Int = FLOAT.sizeOf * 3
    val alignment: Int = VEC4.alignment
  }

  case object VEC4 extends VarType[GLSLVec4] {
    val typeName = "vec4"
    val factory = new GLSLVec4(_)
    val sizeOf: Int = FLOAT.sizeOf * 4
    val alignment: Int = sizeOf
  }

  case object VOID extends VarType[GLSLVoid] {
    val typeName = "void"
    val factory = new GLSLVoid(_)
    val sizeOf = 0
    val alignment = 0
  }
}