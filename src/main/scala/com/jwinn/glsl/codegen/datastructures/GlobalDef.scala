package com.jwinn.glsl.codegen.datastructures

trait GlobalDef {
  private[codegen] val definition: String

  private[codegen] def dependentTypes: Seq[VarType[_]]
}
