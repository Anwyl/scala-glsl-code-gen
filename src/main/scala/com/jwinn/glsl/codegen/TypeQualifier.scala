package com.jwinn.glsl.codegen

/**
 * Type qualifier for a GLSL variable
 * @param glsl The GLSL code associated this qualifier
 */
case class TypeQualifier(glsl: String)
object TypeQualifier {

  /**
   * Qualifier which modifies how values are interpolated between shader stages
   */
  trait InterpolationQualifier

  /**
   * Indicates that a variable shouldn't be interpolated.
   * The value will be exactly the value from the "provoking vertex".
   */
  object FLAT extends TypeQualifier("flat") with InterpolationQualifier

  /**
   * Indicates that a variable should be interpolated linearly in window space.
   */
  object NO_PERSPECTIVE extends TypeQualifier("noperspective") with InterpolationQualifier

  /**
   * Indicates that a variable should be interpolated based on perspective (default).
   */
  object SMOOTH extends  TypeQualifier("smooth") with InterpolationQualifier


  /**
   * Qualifier which modifies the precision of floats.
   * In OpenGL these have no effect, but in GLES they affect the precision of the variables.
   */
  trait PrecisionQualifier
  object HIGHP extends TypeQualifier("highp") with PrecisionQualifier
  object MEDIUMP extends TypeQualifier("mediump") with PrecisionQualifier
  object LOWP extends TypeQualifier("lowp") with PrecisionQualifier


  /**
   * Affects how shader storage blocks can be read/written to.
   * Multiple memory qualifiers are allowed at once.
   */
  trait MemoryQualifier

  /**
   * Indicates that other shader invocations may write to values read by this invocation, and vice versa.
   */
  object COHERENT extends TypeQualifier("coherent") with MemoryQualifier

  /**
   * Indicates that a variable may change at any time, rather than only at memory barriers.
   */
  object VOLATILE extends TypeQualifier("volatile") with MemoryQualifier

  /**
   * Indicates that the value of this variable cannot change due to writing to a different variable.
   */
  object RESTRICT extends TypeQualifier("restrict") with MemoryQualifier

  /**
   * Prevents writing to this variable.
   * Together with WRITE_ONLY, only allows reading variable metadata.
   */
  object READ_ONLY extends  TypeQualifier("readonly") with MemoryQualifier

  /**
   * Prevents reading from this variable
   * Together with READ_ONLY, only allows reading variable metadata.
   */
  object WRITE_ONLY extends TypeQualifier("writeonly") with MemoryQualifier

  /**
   * Macro for READ_ONLY + WRITE_ONLY
   */
  object METADATA_ONLY extends TypeQualifier("readonly writeonly") with MemoryQualifier


  /**
   * Ensures that the same code compiled in different shaders from producing different results in this variable.
   */
  object INVARIANT extends  TypeQualifier("invariant")
}
