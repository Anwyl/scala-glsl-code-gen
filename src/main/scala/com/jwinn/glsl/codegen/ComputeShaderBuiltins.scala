package com.jwinn.glsl.codegen

import com.jwinn.glsl.codegen.types._

/**
 * Built-in variables available within compute shaders
 */
object ComputeShaderBuiltins {
  val gl_NumWorkGroups = new GLSLUVec3("gl_NumWorkGroups")
  val gl_WorkGroupID = new GLSLUVec3("gl_WorkGroupID")
  val gl_LocalInvocationID = new GLSLUVec3("gl_LocalInvocationID")
  val gl_GlobalInvocationID = new GLSLUVec3("gl_GlobalInvocationID")
  val gl_LocalInvocationIndex = new GLSLUInt("gl_LocalInvocationIndex")
}
