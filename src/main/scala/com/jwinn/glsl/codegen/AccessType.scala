package com.jwinn.glsl.codegen

/**
 * Access modifier for a GLSL variable
 */
trait AccessType {
  def glsl: String
}

object AccessType {

  /**
   * Access type "in" denotes a read-only variable
   */
  object IN extends AccessType {
    def glsl = "in"
  }

  /**
   * Access type "out" denotes a write-only variable
   */
  object OUT extends AccessType {
    def glsl = "out"
  }

  /**
   * Access type "uniform" denotes a read-only variable which doesn't change between executions of a shader.
   */
  object UNIFORM extends AccessType {
    def glsl = "uniform"
  }

  /**
   * Access type "buffer" denotes a read/write variable backed by a buffer object.
   * This is the only access type which can contain an unbounded array. Runtime array size is determined by buffer size.
   */
  object BUFFER extends AccessType {
    def glsl = "buffer"
  }

  /**
   * Access type "const" denotes a read-only variable which is a compiletime constant.
   */
  object CONST extends AccessType {
    def glsl = "const"
  }
}
