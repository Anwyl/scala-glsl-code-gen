import com.jwinn.glsl.codegen.datastructures.VarType._
import com.jwinn.glsl.codegen.datastructures.{MemberDef, VarDef}
import com.jwinn.glsl.codegen.structures.{Function0, GLFor, GLIf}
import com.jwinn.glsl.codegen.types.{GLSLValue, GLSLVec3, GLSLVec4}
import com.jwinn.glsl.codegen.{AccessType, FunctionContent, ShaderContent}
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.matching.Regex

class ShaderContentTest extends AnyFlatSpec {
  "Empty constructor" should "return successfully" in {
    new ShaderContent
  }
  it should "use version 430" in {
    val sc = new ShaderContent
    assert(sc.definition.startsWith("#version 430\n"))
  }

  def makeRegex(code: String): Regex = {
    val replacements = Seq("\\", "$", "^", "[", "]", "*", "+", "(", ")", "{", "}").map(a ⇒ (a, '\\' + a)) ++ Seq(("\r", ""), ("\n", " "), ("  ", "\\s+"), (" ", "\\s*"))
    replacements.foldLeft(code) {case (s, (rx, rp)) ⇒ s.replace(rx, rp) }.r
  }

  def testGLSL(glsl: String)(main: FunctionContent[Nothing] ⇒ Unit): Unit = {
    val sc = new ShaderContent
    sc.setMain(main)

    val regex = makeRegex(glsl)
    assert(regex.findFirstIn(sc.definition).isDefined, regex.pattern + " doesn't match " + sc.definition)
  }

  "ShaderContent" should "Support local variables" in {
    testGLSL("void  main ( void ) { float  localVar ; }") { implicit ctx ⇒
      val m = FLOAT("localVar")
    }
  }

  it should "Allow assignment" in {
    testGLSL("void  main ( void ) { double localVar ; localVar = 1.0 ; }") { implicit ctx ⇒
      import com.jwinn.glsl.codegen.GLSLImplicits._
      val m = DOUBLE("localVar")
      (m := 1d).`;`
    }
  }

  it should "Allow addition" in {
    testGLSL("void  main ( void ) { int  localVar1 ; int  localVar2 ; localVar1 = ( localVar1 + localVar2 ) ; }") { implicit ctx ⇒
      val m1 = INT("localVar1")
      val m2 = INT("localVar2")
      (m1 := m1 + m2).`;`
    }
  }

  it should "Allow multiplication" in {
    testGLSL("void  main ( void ) { int  localVar1 ; int  localVar2 ; localVar1 = ( localVar1 * localVar2 ) ; }") { implicit ctx ⇒
      val m1 = INT("localVar1")
      val m2 = INT("localVar2")
      (m1 := m1 * m2).`;`
    }
  }

  it should "Allow division" in {
    testGLSL("void  main ( void ) { int  localVar1 ; int  localVar2 ; localVar1 = ( localVar1 / localVar2 ) ; }") { implicit ctx ⇒
      val m1 = INT("localVar1")
      val m2 = INT("localVar2")
      (m1 := m1 / m2).`;`
    }
  }

  it should "Allow unrolled loops" in {
    testGLSL("void  main ( void ) { int  localVar1 ; localVar1 = ( localVar1 + 1 ) ; localVar1 = ( localVar1 + 1 ) ; localVar1 = ( localVar1 + 1 ) ; }") { implicit ctx ⇒
      import com.jwinn.glsl.codegen.GLSLImplicits._
      val m1 = INT("localVar1")
      for (_ ← 0 until 3) {
        (m1 := m1 + 1).`;`
      }
    }
  }

  it should "Allow for loops" in {
    testGLSL("void  main ( void ) { uint  localVar1 ; for ( uint  i = 0 ; ( i < 3 ); ( i ++ ) ) { localVar1 = ( localVar1 + i ) ; } }") { implicit ctx ⇒
      import scala.language.postfixOps
      val m1 = UINT("localVar1")
      GLFor[Nothing]("i", 0, 3) { (context, i) ⇒
        implicit val ctx: FunctionContent[Nothing] = context
        (m1 := m1 + i).`;`
      }
    }
  }

  it should "Allow if statements" in {
    testGLSL("void  main ( void ) { uint  localVar1 ; if ( ( localVar1 < 2 ) ) { ( localVar1 ++ ) ; } }") { implicit ctx ⇒
      import com.jwinn.glsl.codegen.GLSLImplicits._

      import scala.language.postfixOps
      val m1 = UINT("localVar1")
      GLIf(m1 < 2) { implicit ctx ⇒
        (m1++).`;`
      }
    }
  }

  it should "Allow global variables" in {
    val globalVar1 = VarDef(AccessType.UNIFORM, MemberDef("globalVar1", INT))
    val globalVar2 = VarDef(AccessType.IN, MemberDef("globalVar2", INT))
    val globalVar3 = VarDef(AccessType.OUT, MemberDef("globalVar3", INT))

    testGLSL("uniform  int  globalVar1; in  int  globalVar2; out  int  globalVar3; void  main ( void ) { globalVar3 = ( globalVar1 + globalVar2 ) ; }") { implicit ctx ⇒
      val g1 = globalVar1.inContext
      val g2 = globalVar2.inContext
      val g3 = globalVar3.inContext
      (g3 := g1 + g2).`;`
    }
  }

  it should "Handle complex shaders" in {

    import AccessType._
    import com.jwinn.glsl.codegen.BuiltinFunctions._
    import com.jwinn.glsl.codegen.GLSLImplicits._
    import com.jwinn.glsl.codegen.VertShaderBuiltins._
    val hasVec4Color = true
    val hasVec3Color = false
    val hasNormals = true
    val hasTangents = true
    val hasUV1 = true
    val hasUV2 = true

    val a_Position = VarDef(IN, MemberDef("a_Position", VEC3))
    val v_Position = VarDef(OUT, MemberDef("v_Position", VEC3))
    val a_Normal = VarDef(IN, MemberDef("a_Normal", VEC3))
    val a_Tangent = VarDef(IN, MemberDef("a_Tangent", VEC4))
    val v_TBN = VarDef(OUT, MemberDef("v_TBN", MAT3))
    val v_Normal = VarDef(OUT, MemberDef("v_Normal", VEC3))
    val a_UV1 = VarDef(IN, MemberDef("a_UV1", VEC2))
    val a_UV2 = VarDef(IN, MemberDef("a_UV2", VEC2))
    val v_UVCoord1 = VarDef(OUT, MemberDef("v_UVCoord1", VEC2))
    val v_UVCoord2 = VarDef(OUT, MemberDef("v_UVCoord2", VEC2))
    val u_ViewProjectionMatrix = VarDef(UNIFORM, MemberDef("u_ViewProjectionMatrix", MAT4))
    val u_ModelMatrix = VarDef(UNIFORM, MemberDef("u_ModelMatrix", MAT4))
    val u_NormalMatrix = VarDef(UNIFORM, MemberDef("u_NormalMatrix", MAT4))

    def getPosition[T <: GLSLValue[T]]()(implicit ctx: FunctionContent[T]): GLSLVec4 = Function0("getPosition", VEC4) { implicit ctx ⇒
      VEC4("pos", vec4(a_Position.inContext, 1f))
    }()

    def getNormal[T <: GLSLValue[T]]()(implicit ctx: FunctionContent[T]): GLSLVec3 = Function0("getNormal", VEC3) { implicit ctx ⇒
      val normal = VEC3("normal", a_Normal.inContext)
      normalize(normal)
    }()

    def getTangent[T <: GLSLValue[T]]()(implicit ctx: FunctionContent[T]): GLSLVec3 = Function0("getTangent", VEC3) { implicit ctx ⇒
      val tangent = VEC3("tangent", a_Tangent.inContext.xyz)
      normalize(tangent)
    }()

    testGLSL(
    """
uniform  mat4  u_ModelMatrix ;
in  vec3  a_Position ;
out  vec3  v_Position ;
in  vec4  a_Tangent ;
uniform  mat4  u_NormalMatrix ;
in  vec3  a_Normal ;
out  mat3  v_TBN ;
out  vec2  v_UVCoord1 ;
in  vec2  a_UV1 ;
out  vec2  v_UVCoord2 ;
in  vec2  a_UV2 ;
out  vec4  v_Color ;
in  vec4  a_Color ;
uniform  mat4  u_ViewProjectionMatrix ;
vec4  getPosition ( ) {
vec4  pos = vec4 ( a_Position , 1.0 ) ;
return  pos ;
}
vec3  getTangent ( ) {
vec3  tangent = a_Tangent.xyz ;
return  normalize ( tangent ) ;
}
vec3  getNormal ( ) {
vec3  normal = a_Normal ;
return  normalize ( normal ) ;
}
void  main ( void ) {
vec4  pos = ( u_ModelMatrix * getPosition ( ) ) ;
v_Position = ( pos.xyz / pos.w ) ;
vec3  tangent = getTangent ( ) ;
vec3  normalW = normalize ( vec3 ( ( u_NormalMatrix * vec4 ( getNormal ( ) , 0.0 ) ) ) ) ;
vec3  tangentW = normalize ( vec3 ( ( u_ModelMatrix * vec4 ( tangent , 0.0 ) ) ) ) ;
vec3  bitangentW = cross ( normalW , tangentW ) ;
v_TBN = mat3 ( tangentW , bitangentW , normalW ) ;
v_UVCoord1 = a_UV1 ;
v_UVCoord2 = a_UV2 ;
v_Color = a_Color ;
gl_Position = ( u_ViewProjectionMatrix * pos ) ;
}
"""
    ) { implicit ctx ⇒
      val pos = VEC4("pos", u_ModelMatrix.inContext * getPosition())
      (v_Position.inContext := pos.xyz / pos.w).`;`

      if (hasNormals) {
        if (hasTangents) {
          val tangent = VEC3("tangent", getTangent())
          val normalW = VEC3("normalW", normalize(vec3(u_NormalMatrix.inContext * vec4(getNormal(), 0f))))
          val tangentW = VEC3("tangentW", normalize(vec3(u_ModelMatrix.inContext * vec4(tangent, 0f))))
          val bitangentW = VEC3("bitangentW", cross(normalW, tangentW))
          (v_TBN.inContext := mat3(tangentW, bitangentW, normalW)).`;`
        } else {
          (v_Normal.inContext := normalize(vec3(u_NormalMatrix.inContext * vec4(getNormal(), 0f)))).`;`
        }
      }

      (v_UVCoord1.inContext := (if (hasUV1) a_UV1.inContext else vec2(0f, 0f))).`;`
      (v_UVCoord2.inContext := (if (hasUV2) a_UV2.inContext else vec2(0f, 0f))).`;`

      if (hasVec3Color) {
        (VarDef(OUT, MemberDef("v_Color", VEC3)).inContext := VarDef(IN, MemberDef("a_Color", VEC3)).inContext).`;`
      } else if (hasVec4Color) {
        (VarDef(OUT, MemberDef("v_Color", VEC4)).inContext := VarDef(IN, MemberDef("a_Color", VEC4)).inContext).`;`
      }
      (gl_Position := u_ViewProjectionMatrix.inContext * pos).`;`
    }
  }
}
